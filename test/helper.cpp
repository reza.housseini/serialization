#include <boost/ut.hpp>
#include <cstdint>
#include "common/helper.hpp"
#include "common/incident.hpp"
#include <boost/hana.hpp>

int main()
{
  using namespace boost::ut;
  using namespace mecos::common; 
  namespace hana = boost::hana;

  struct Bar
  {
    BOOST_HANA_DEFINE_STRUCT(Bar,
        (std::uint16_t, x),
        (float, y));
  };

  struct Foo
  {
    BOOST_HANA_DEFINE_STRUCT(Foo,
        (std::uint32_t, a),
        (std::int8_t, b),
        (Bar, c),
        (incident::Timestamp, d));
  };

  "fill struct with zero"_test = []
  {
    Foo foo{};
    foo.a = 112;
    foo.d.CycleTime = 1321;
    fill(foo, 0);

    expect(foo.a == 0);
    expect(foo.d.CycleTime == 0);
  };

  "fill struct with 111"_test = []
  {
    Foo foo{};
    foo.a = 112;
    foo.d.CycleTime = 1321;
    fill(foo, 111);

    expect(foo.a == 111);
    expect(foo.c.x == 111);
    expect(foo.d.CycleTime == 111);
  };

  "compare structs"_test = []
  {
    Foo foo1{0, 1, {2, 3}, {{4, 5, 6, 7, 8, 9}, 10}};
    Foo foo2{0, 1, {2, 3}, {{4, 5, 6, 7, 8, 9}, 10}};
    Foo foo3{0, 1, {2, 3}, {{4, 5, 6, 7, 11, 9}, 10}};

    expect(compare(foo1, foo2));
    expect(!compare(foo1, foo3));
  };

};
