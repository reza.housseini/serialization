#include "common/list.hpp"
#include <boost/ut.hpp>

int
main()
{
  using namespace boost::ut;

  "Create List : push pop size"_test = [] {
    mecos::common::List<int32_t, 30> list1;
    expect(list1.size() == 0_ul and list1.max_size() == 30_ul);

    list1.pushBack(31);
    list1.pushBack(32);
    list1.pushFront(11);
    expect(list1.size() == 3_ul and list1.max_size() == 30_ul);
    expect(11_i == list1.front() and 32_i == list1.back());

    list1.pushFront(10);
    list1.pushBack(33);
    expect(list1.size() == 5_ul and list1.max_size() == 30_ul);
    expect(10_i == list1.front() and 33_i == list1.back());

    list1.popBack();
    expect(list1.size() == 4_ul and list1.max_size() == 30_ul);
    expect(10_i == list1.front() and 32_i == list1.back());

    list1.popFront();
    expect(list1.size() == 3_ul and list1.max_size() == 30_ul);
    expect(11_i == list1.front() and 32_i == list1.back());

    list1.popFront();
    list1.popBack();
    list1.popBack();
    expect(list1.size() == 0_ul and list1.max_size() == 30_ul);

    list1.pushFront(77);
    expect(list1.size() == 1_ul and list1.max_size() == 30_ul);
    expect(77_i == list1.front() and 77_i == list1.back());

    list1.popFront();
    expect(list1.size() == 0_ul and list1.max_size() == 30_ul);
  };

  "Insert / Erase / []"_test = [] {
    mecos::common::List<int16_t, 60> list1;
    expect(list1.size() == 0_ul and list1.max_size() == 60_ul);

    list1.pushFront(10);
    expect(list1.size() == 1_ul and list1.max_size() == 60_ul);
    expect(10_i == list1.front() and 10_i == list1.back());
    expect(list1[0] == 10_i);

    list1.pushBack(30);
    list1.pushFront(9);
    list1.pushFront(8);
    expect(list1.size() == 4_ul and list1.max_size() == 60_ul);
    expect(8_i == list1.front() and 30_i == list1.back());
    expect(list1[0] == 8_i and list1[2] == 10_i and list1[3] == 30_i);

    list1.insert(88, 2);
    expect(list1.size() == 5_ul and list1.max_size() == 60_ul);
    expect(8_i == list1.front() and 30_i == list1.back());
    expect(list1[1] == 9_i and list1[2] == 88_i and list1[3] == 10_i);

    list1.insert(77, 0);
    expect(list1.size() == 6_ul and list1.max_size() == 60_ul);
    expect(77_i == list1.front() and 30_i == list1.back());
    expect(list1[0] == 77_i and list1[1] == 8_i and list1[5] == 30_i);

    list1.insert(99, 6);
    expect(list1.size() == 7_ul and list1.max_size() == 60_ul);
    expect(77_i == list1.front() and 99_i == list1.back());
    expect(list1[0] == 77_i and list1[5] == 30_i and list1[6] == 99_i);

    list1.erase(4);
    expect(list1.size() == 6_ul and list1.max_size() == 60_ul);
    expect(77_i == list1.front() and 99_i == list1.back());
    expect(list1[3] == 88_i and list1[4] == 30_i and list1[5] == 99_i);

    list1.erase(0);
    expect(list1.size() == 5_ul and list1.max_size() == 60_ul);
    expect(8_i == list1.front() and 99_i == list1.back());
    expect(list1[0] == 8_i and list1[1] == 9_i and list1[4] == 99_i);

    list1.erase(4);
    expect(list1.size() == 4_ul and list1.max_size() == 60_ul);
    expect(8_i == list1.front() and 30_i == list1.back());
    expect(list1[0] == 8_i and list1[2] == 88_i and list1[3] == 30_i);

    list1.insert(33, 1);
    expect(list1.size() == 5_ul and list1.max_size() == 60_ul);
    expect(8_i == list1.front() and 30_i == list1.back());
    expect(list1[0] == 8_i and list1[1] == 33_i and list1[2] == 9_i);

    list1.popBack();
    list1.popFront();
    expect(list1.size() == 3_ul and list1.max_size() == 60_ul);
    expect(33_i == list1.front() and 88_i == list1.back());
    expect(list1[0] == 33_i and list1[1] == 9_i and list1[2] == 88_i);

    list1.erase(1);
    list1.erase(0);
    list1.erase(0);
    expect(list1.size() == 0_ul and list1.max_size() == 60_ul);

    list1.insert(44, 0);
    expect(list1.size() == 1_ul and list1.max_size() == 60_ul);
    expect(44_i == list1.front() and 44_i == list1.back());
    expect(list1[0] == 44_i);

    list1.erase(0);
    expect(list1.size() == 0_ul and list1.max_size() == 60_ul);
  };
}
