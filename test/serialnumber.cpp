#include "common/serialnumber.hpp"
#include <boost/ut.hpp>
#include <cstdint>

namespace common = mecos::common;

int
main()
{
  using namespace boost::ut;

  "serialnumber_manipulation"_test = [] {
    std::string value;
    common::Serialnumber sn;

    if (auto&& sn_opt = common::Serialnumber::construct("ACCWW"))
    {
      sn = *sn_opt;
      value = sn.to_string();
    }
    expect("ACCWW" == value);
  };

  "wrong serialnumber"_test = [] {
    auto&& sn_opt1 = common::Serialnumber::construct("ACCWWW");
    expect(sn_opt1 == std::nullopt);

    auto&& sn_opt2 = common::Serialnumber::construct("ACCW1");
    expect(sn_opt2 == std::nullopt);
  };
}
