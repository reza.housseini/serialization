#include <boost/ut.hpp>
#include "common/variable.hpp"
#include "common/utility.hpp"
#include "common/types.hpp"
#include <string_view>

int main()
{
  using namespace boost::ut;
  using namespace mecos::common;

  "initialize variable"_test = []
  {
    Variable<> v{0, "dasda", {0, 10}, getTypeCode<std::int32_t>(TypesMap), Rate::fast, Unit{}, "asdasdasdasd"};
    expect("dasda" == std::string_view(v.name.data(), v.name.size()));
  };
}
