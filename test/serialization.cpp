#include "common/serialization.hpp"
#include "common/page.hpp"
#include <boost/ut.hpp>
#include <cmath>
#include <cstdint>
#include <cstring>
#include <iterator>
#include <string_view>

int
main()
{
  using namespace boost::ut;
  using namespace mecos::common;
  namespace hana = boost::hana;

  "serialize uint32_t"_test = [] {
    std::uint32_t in = 42;
    std::uint32_t out = 0;
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(1_ul == out_size);
    expect(in == out);
  };

  "serialize array<uint32_t>"_test = [] {
    std::array<std::uint32_t, 5> in{ 11, 12, 13, 14, 15 };
    std::array<std::uint32_t, 5> out;
    std::array<std::uint32_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(5_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(5_ul == out_size);
    expect(in == out);
  };

  "serialize uint16_t"_test = [] {
    std::uint16_t in = 42;
    std::uint16_t out = 0;
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(1_ul == out_size);
    expect(in == out);
  };

  "serialize array<uint16_t>"_test = [] {
    std::array<std::uint16_t, 5> in{ 11, 12, 13, 14, 15 };
    std::array<std::uint16_t, 5> out;
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(5_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(5_ul == out_size);
    expect(in == out);
  };

  "serialize float"_test = [] {
    float in = 42.0;
    float out = 0;
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(1_ul == out_size);
    expect(in == out);
  };

  "serialize TripleFloat16"_test = [] {
    TripleFloat16 in{ 12.3f, 86.5f, -76.34f };
    TripleFloat16 out;
    std::array<page::DataBase_t, 20> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(2_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(2_ul == out_size);
    expect(abs(in.float32[0] - out.float32[0]) < 0.5f);
    expect(abs(in.float32[1] - out.float32[1]) < 0.5f);
    expect(abs(in.float32[2] - out.float32[2]) < 0.5f);
  };

  "serialize TripleFloat32"_test = [] {
    TripleFloat32 in{ 12.3f, 86.5f, -76.34f };
    TripleFloat32 out;
    std::array<page::DataBase_t, 20> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(3_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(3_ul == out_size);
    expect(in[0] == out[0]);
    expect(in[1] == out[1]);
    expect(in[2] == out[2]);
  };

  "serialize bool"_test = [] {
    bool in = true;
    bool out = false;
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(1_ul == out_size);
    expect(in == out);
  };

  "serialize char"_test = [] {
    char in = 'z';
    char out = 'a';
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(1_ul == out_size);
    expect(in == out);
  };

  "serialize string"_test = [] {
    std::string in = "asdasd";
    std::string out = "";
    std::array<page::DataBase_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(7_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(7_ul == out_size);
    expect(in == out);
  };

  "serialize struct"_test = [] {
    struct A
    {
      BOOST_HANA_DEFINE_STRUCT(A, (std::uint8_t, b), (std::string, name));
    };

    A in{ 8, "asdasd" };
    A out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(8_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(8_ul == out_size);
    expect(8_i == out.b);
    expect("asdasd" == out.name);
  };

  "serialize char array"_test = [] {
    std::array<char, 10> in{ "abcdefghi" };
    std::array<char, 10> out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(10_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(10_ul == out_size);
    expect("abcdefghi" == std::string_view(out.data(), out.size() - 1));
  };

  "serialize variable"_test = [] {
    Variable<> in{
      1,          "dasda", { 0, 10 },     getTypeCode<std::int32_t>(TypesMap),
      Rate::fast, Unit{},  "asdasdasdasd"
    };
    Variable<std::string> out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(25_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(25_ul == out_size);
    expect("dasda" == out.name);
  };

  "serialize variable array"_test = [] {
    std::array<Variable<>, 2> in = { { { 1,
                                         "dasda",
                                         { 0, 10 },
                                         getTypeCode<std::int32_t>(TypesMap),
                                         Rate::fast,
                                         Unit{},
                                         "asdasdasdasd" },
                                       { 2,
                                         "dasda",
                                         { 0, 10 },
                                         getTypeCode<std::int16_t>(TypesMap),
                                         Rate::fast,
                                         Unit{},
                                         "asdasdasdasd" } } };
    std::array<Variable<std::string>, 2> out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(50_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(50_ul == out_size);
    expect("dasda" == out[0].name);
  };

  "serialize incident::Timestamp"_test = [] {
    incident::Timestamp in = { { 1, 2, 3, 4, 5, 6 }, 9999 };
    incident::Timestamp out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(2_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(2_ul == out_size);
    expect(1_u == out.DateAndTime.time.Sec);
    expect(6_u == out.DateAndTime.time.Year);
    expect(9999_u == out.CycleTime);
  };

  "serialize incident::FindSpecification"_test = [] {
    incident::FindSpecification in = { { { 1, 2, 3, 4, 5, 6 }, 9999 },
                                       incident::Levels::Fault,
                                       55 };
    incident::FindSpecification out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(4_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(4_ul == out_size);
    expect(1_u == out.TimesOfIncident.DateAndTime.time.Sec);
    expect(6_u == out.TimesOfIncident.DateAndTime.time.Year);
    expect(incident::Levels::Fault == out.LevelOfIncident);
    expect(55_s == out.NumberOfIncidents);
  };

  "serialize incident::LogDefinition"_test = [] {
    incident::LogDefinition in = { 66,
                                   { { 1, 2, 3, 4, 5, 6 }, 9999 },
                                   { 7, 8, 9, 10 } };
    incident::LogDefinition out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(7_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(7_ul == out_size);
    expect(66_u == out.id);
    expect(1_u == out.time.DateAndTime.time.Sec);
    expect(6_u == out.time.DateAndTime.time.Year);
    expect(7_u == out.attributes[0]);
    expect(10_u == out.attributes[3]);
  };

  "serialize FindLogdata"_test = [] {
    FindLogdata in = { { { 1, 2, 3, 4, 5, 6 }, 9999 }, 7 };
    FindLogdata out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(3_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(3_ul == out_size);
    expect(7_u == out.NumberOfData);
    expect(1_u == out.TimeOfData.DateAndTime.time.Sec);
    expect(6_u == out.TimeOfData.DateAndTime.time.Year);
  };

  "pack uint16_t to uint32_t"_test = [] {
    std::int8_t a = -34;
    std::uint32_t b = 142;
    std::uint32_t c = pack<std::uint16_t, std::uint32_t>(
      { static_cast<std::uint16_t>(a), static_cast<std::uint16_t>(b) });
    auto [d, e] = unpack<std::uint32_t, std::uint16_t>(c);
    expect(a == static_cast<int8_t>(d));
    expect(b == e);
  };

  "serialize int32_t with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    std::int32_t in = 3345;
    std::int32_t out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(2_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(2_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<std::int32_t>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<int32_t>(outFactory) == in);
    expect(factoryIndex == indexIn);
  };

  "serialize int16_t with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    std::int16_t in = -314;
    std::int16_t out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(1_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<std::int16_t>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<int16_t>(outFactory) == in);
    expect(factoryIndex == indexIn);
  };

  "serialize uint16_t with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    std::uint16_t in = 314;
    std::uint16_t out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(1_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<std::uint16_t>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<uint16_t>(outFactory) == in);
    expect(factoryIndex == indexIn);
  };

  "serialize int8_t with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    std::int8_t in = -12;
    std::int8_t out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(1_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<std::int8_t>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<int8_t>(outFactory) == in);
    expect(factoryIndex == indexIn);
  };

  "serialize uint8_t with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    std::uint8_t in = 12;
    std::uint8_t out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(1_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<std::uint8_t>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<uint8_t>(outFactory) == in);
    expect(factoryIndex == indexIn);
  };

  "serialize TripleFloat32 with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    TripleFloat32 in{ 12.3f, 33.2f, 87.98f };
    TripleFloat32 out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(4_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(4_ul == out_size);
    expect(indexIn == indexOut);
    expect(in[0] == out[0]);
    expect(in[1] == out[1]);
    expect(in[2] == out[2]);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<TripleFloat32>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(std::get<TripleFloat32>(outFactory)[0] == in[0]);
    expect(std::get<TripleFloat32>(outFactory)[1] == in[1]);
    expect(std::get<TripleFloat32>(outFactory)[2] == in[2]);
    expect(factoryIndex == indexIn);
  };

  "serialize TripleFloat16 with index"_test = [] {
    VarIndex_t indexIn = 11;
    VarIndex_t indexOut;
    TripleFloat16 in{ 12.3f, 33.2f, 87.98f };
    TripleFloat16 out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(2_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(2_ul == out_size);
    expect(indexIn == indexOut);
    expect(abs(in.float32[0] - out.float32[0]) < 0.5f);
    expect(abs(in.float32[1] - out.float32[1]) < 0.5f);
    expect(abs(in.float32[2] - out.float32[2]) < 0.5f);
    // TODO umb:die Genauigkeit sollte hier nochmals überprüft werden!
    // serialization.cpp:346:FAILED [33.2 == 33]
    // serialization.cpp:347:FAILED [87.98 == 87.5]

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);

    VarIndex_t factoryIndex;
    std::uint8_t typeCode = getTypeCode<TripleFloat16>(TypesMap);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(factoryIndex, typeCode);
    auto [out_offset, outFactory] = deserializer(buffer.cbegin());
    expect(out_offset == out_size);
    expect(abs(std::get<TripleFloat16>(outFactory).float32[0] - in.float32[0]) <
           0.5f);
    expect(abs(std::get<TripleFloat16>(outFactory).float32[1] - in.float32[1]) <
           0.5f);
    expect(abs(std::get<TripleFloat16>(outFactory).float32[2] - in.float32[2]) <
           0.5f);
    expect(factoryIndex == indexIn);
  };

  "serialize array of float with index"_test = [] {
    VarIndex_t indexIn = 99;
    VarIndex_t indexOut;
    std::array<float, 4> in{ 3.f, 3.7f, 4.f, 5.4f };
    std::array<float, 4> out;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(indexIn, in, buffer.begin());
    expect(5_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), indexOut, out);
    expect(5_ul == out_size);
    expect(indexIn == indexOut);
    expect(in == out);

    VarIndex_t onlyIndex;
    deserialize(buffer.cbegin(), onlyIndex);
    expect(onlyIndex == indexIn);
  };

  "serialize crc32"_test = [] {
    crc32 crcIn{ 123123 };
    crc32 crcOut;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(crcIn, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), crcOut);
    expect(1_ul == out_size);
    expect(crcIn == crcOut);
  };

  "serialize page::HeaderCode"_test = [] {
    page::HeaderCode codeIn =
      getTypeCode<page::XrtHeader>(page::HeaderTypesMap);
    page::HeaderCode codeOut;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(codeIn, buffer.begin());
    expect(1_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), codeOut);
    expect(1_ul == out_size);
    expect(codeIn == codeOut);
  };

  "serialize page::XrtHeader"_test = [] {
    page::XrtHeader headerIn = { { 'A', 'B', 'C', 'D', 'E' },
                                 5,
                                 6,
                                 { { 1, 2, 3, 4, 5, 6 }, 88 },
                                 crc32{ 12345678 } };
    page::XrtHeader headerOut;
    std::array<page::DataBase_t, 128> buffer;
    auto in_size = serialize(headerIn, buffer.begin());
    expect(10_ul == in_size);
    auto out_size = deserialize(buffer.cbegin(), headerOut);
    expect(10_ul == out_size);
    /* expect(compare(headerIn, headerOut)); */
  };

  "serializer factory"_test = [] {
    VarIndex_t index = 90;
    std::uint8_t typeCode = getTypeCode<std::int32_t>(TypesMap);
    std::int32_t in_data = 3345;
    DataTypesPointer in = &in_data;
    std::array<page::DataBase_t, 128> buffer;
    auto [serializer, inTypeSize] =
      serializerFactory<decltype(buffer.begin())>(index, typeCode, in);
    expect(2_ul == inTypeSize);
    std::size_t in_offset = serializer(buffer.begin());
    expect(2_ul == in_offset);
    VarIndex_t outIndex;
    deserialize(buffer.cbegin(), outIndex);
    expect(index == outIndex);
    in_offset += serializer(std::next(buffer.begin(), in_offset));
    expect(4_ul == in_offset);
    auto [deserializer, outTypeSize] =
      deserializerFactory<decltype(buffer.cbegin())>(index, typeCode);
    expect(2_ul == outTypeSize);
    auto [out_offset, out] = deserializer(buffer.cbegin());
    auto out_data = std::get<std::int32_t>(out);
    expect(2_ul == out_offset);
    expect(out_data == in_data);
    auto [out_offset2, out2] =
      deserializer(std::next(buffer.cbegin(), out_offset));
    expect(2_ul == out_offset2);
    auto out_data2 = std::get<std::int32_t>(out2);
    expect(out_data2 == in_data);
  };
}
