#include "common/pcutils.hpp"
#include <boost/ut.hpp>
#include <limits>

template<typename T,
         typename U,
         std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
bool
compare(T lhs, U rhs)
{
  return lhs == static_cast<T>(rhs);
}

template<typename T,
         typename U,
         std::enable_if_t<!std::is_arithmetic<T>::value, int> = 0>
bool compare(T, U)
{
  return false;
}

int
main()
{
  using namespace boost::ut;
  namespace mc = mecos::common;
  std::array<mc::Variable<std::string>, 8> xrt{
    { { 0,
        "speedcpu",
        { 0, 10 },
        mc::getTypeCode<std::int32_t>(mc::TypesMap),
        mc::Rate::fast,
        mc::Unit{},
        "Mechanical Rotor Speed" },
      { 1,
        "U_Z",
        { 0, 20 },
        mc::getTypeCode<float>(mc::TypesMap),
        mc::Rate::slow,
        mc::Unit{},
        "DC-Bus Voltage" },
      { 2,
        "I_D",
        { 0, 30 },
        mc::getTypeCode<std::uint16_t>(mc::TypesMap),
        mc::Rate::fast,
        mc::Unit{},
        "Motor Direct Current" },
      { 3,
        "I_Q",
        { 0, 40 },
        mc::getTypeCode<std::int16_t>(mc::TypesMap),
        mc::Rate::fast,
        mc::Unit{},
        "Motor Quadrature Current" },
      { 4,
        "U_DCM",
        { 0, 50 },
        mc::getTypeCode<std::uint8_t>(mc::TypesMap),
        mc::Rate::slow,
        mc::Unit{},
        "DC-Bus Midpoint" },
      { 5,
        "Ku",
        { 0, 60 },
        mc::getTypeCode<std::int8_t>(mc::TypesMap),
        mc::Rate::slow,
        mc::Unit{},
        "Motor Back-EMF Constant" },
      { 6,
        "J",
        { 0, 70 },
        mc::getTypeCode<std::uint32_t>(mc::TypesMap),
        mc::Rate::slow,
        mc::Unit{},
        "Rotor Moment Of Intertia" },
      { 7,
        "readvar",
        { 0, 80 },
        mc::getTypeCode<mc::ReadvarData>(mc::TypesMap),
        mc::Rate::fast,
        mc::Unit{},
        "Readvar RPC" } }
  };

  "varIndexToTypeCode"_test = [&] {
    auto typeCode = mc::varIndexToTypeCode(3, xrt);
    expect(typeCode == std::uint8_t{ 0xd1 });
  };

  "varIndexToVarName"_test = [&] {
    auto varname = mc::varIndexToVarName(3, xrt);
    expect(varname == "I_Q");
  };

  "varNameToVarIndex"_test = [&] {
    auto varIndex = mc::varNameToVarIndex("I_Q", xrt);
    expect(varIndex == 3);
  };

  "varNameToTypeCode"_test = [&] {
    auto typeCode = mc::varNameToTypeCode("I_Q", xrt);
    expect(typeCode == std::uint8_t{ 0xd1 });
  };

  "payload to buffer"_test = [] {
    std::string_view content{ "mystring1234" };
    auto buffer = mc::payloadToBuffer(content);
    expect(std::is_same<std::uint32_t const*, decltype(buffer)>::value);

    std::array<std::uint32_t, 3> bufferOut;
    std::copy(buffer, buffer + 3, bufferOut.begin());

    std::string out = mc::bufferToPayload(bufferOut);
    expect(content.size() == 12);
    expect(out.size() == 12);
    expect(out == content);
  };

  "filter"_test = [&] {
    auto reducedXrt = mc::filter(
      xrt.begin(), xrt.end(), [](auto var) { return var.index < 7; });
    expect(reducedXrt.size() == 7);
  };

  "map"_test = [&] {
    auto result =
      mc::map(xrt.begin(), xrt.end(), [](auto var) { return var.name; });
    expect(result[1] == "U_Z");
  };

  "index_if"_test = [&] {
    auto index = mc::indexIf(
      xrt.begin(), xrt.end(), [](auto var) { return var.index == 6; });

    std::vector<uint32_t> numbers = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 5, 5, 2, 2 };
    auto indices = mc::indicesIf(
      numbers.begin(), numbers.end(), [](auto elem) { return elem == 5; });
    expect(index == 6);
    expect(indices.size() == 3);
    expect(indices[1] == 9);
  };

  "typeCode to varSize"_test = [xrt] {
    uint8_t typeCode = mc::varIndexToTypeCode(3, xrt); // std::int16_t
    expect(typeCode == 0xd1);
    size_t varSize = mc::typeCodeToVarSize(typeCode);
    expect(varSize == 1);

    typeCode = mc::varIndexToTypeCode(6, xrt); // std::uint32_t
    expect(typeCode == 0xce);
    varSize = mc::typeCodeToVarSize(typeCode);
    expect(varSize == 1);
  };

  "unwrap time"_test = [&] {
    mc::incident::CycleTime limit =
      std::numeric_limits<mc::incident::CycleTime>::max() - 3;
    std::vector<mc::incident::CycleTime> time;
    for (int i = 0; i < 10; i++)
    {
      time.push_back(limit++);
    }
    time.push_back(time.back() + 2);
    time.push_back(time.back() + 2);
    time.push_back(time.back() + 2);

    std::vector<std::size_t> unrwappedTime = mc::unwrapTime(time);
    auto it = std::adjacent_find(
      unrwappedTime.begin(),
      unrwappedTime.end(),
      [&](auto& lhs, auto& rhs) -> bool { return rhs < lhs; });
    expect(it == unrwappedTime.end());
  };

  "subsampling time"_test = [&] {
    std::vector<std::size_t> time = { 0, 1, 2, 3, 4, 5, 6, 7, 8, 9 };
    std::uint32_t samplingTime = 2;
    std::vector<std::int32_t> reduceIndices =
      mc::findSubSamplingIndices(time, samplingTime);
    expect(reduceIndices.size() == 5);
  };

  "variable index to aligned size"_test = [&] {
    auto s1 = mc::varIndexToAlignedSize(0, xrt);
    expect(s1 == 1_ul);
    auto s2 = mc::varIndexToAlignedSize(7, xrt);
    expect(s2 == 3_ul);
  };

  "cast"_test = [&] {
    auto lambda = [](float value, uint8_t typeCode) {
      bool flag;
      hana::for_each(mc::TypesMap, hana::fuse([&](auto type, auto code) {
                       if (typeCode == code)
                       {
                         using T = typename decltype(type)::type;
                         T var = mc::cast<T>(value);
                         flag = compare(var, value);
                       }
                     }));
      return flag;
    };
    expect(lambda(3.4, 0xce));
    expect(lambda(3.4, 0x2d));
    expect(lambda(3.4, 0xcd));
    expect(lambda(3.4, 0xd1));
    expect(lambda(3.4, 0xca));
    expect(lambda(3.4, 0xc0));
  };
}
