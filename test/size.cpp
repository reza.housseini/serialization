#include "common/size.hpp"
#include "common/incident.hpp"
#include "common/page.hpp"
#include "common/serialization.hpp"
#include "common/types.hpp"
#include "common/variable.hpp"
#include <boost/ut.hpp>
#include <cstdint>
#include <cstring>
#include <iterator>
#include <string_view>

int
main()
{
  using namespace boost::ut;
  using namespace mecos::common;
  namespace hana = boost::hana;
  std::array<page::DataBase_t, 250> buffer;

  "bytesize constexpr"_test = [] {
    static_assert(bytesize<std::uint32_t>() == 4);
    constexpr auto s = bytesize<std::array<std::uint32_t, 345>>();
    static_assert(s == 345 * 4);
    static_assert(bytesize<std::array<std::uint8_t, s>>() == 345 * 4);

    struct Foo
    {
      BOOST_HANA_DEFINE_STRUCT(Foo,
                               (std::uint8_t, a),
                               (std::uint16_t, b),
                               (std::uint32_t, c),
                               (std::uint64_t, d));
    };

    static_assert(bytesize<Foo>() == 15);

    static_assert(bytesize<incident::Timestamp>() == 8);
  };

  "bytesize of std::string_view"_test = [] {
    constexpr std::string_view s{ "haha" };
    static_assert(bytesize(s) == 4);
  };

  "bytesize"_test = [] {
    struct Foo
    {
      BOOST_HANA_DEFINE_STRUCT(Foo,
                               (std::uint8_t, a),
                               (std::uint16_t, b),
                               (std::uint32_t, c),
                               (std::uint64_t, d),
                               (std::string_view, s));
    };

    constexpr Foo foo{ 1, 2, 3, 4, "haha" };

    expect(19_ul == bytesize(foo));

    expect(bytesize<TripleFloat16>() == 6);
    TripleFloat16 tmp{ 11, 12, 13 };
    expect(bytesize<TripleFloat16>(tmp) == 6);

    expect(bytesize<TripleFloat32>() == 12);
  };

  "alignedsize constexpr"_test = [] {
    struct Bar
    {
      BOOST_HANA_DEFINE_STRUCT(Bar, (std::uint8_t, a), (std::uint16_t, b));
    };

    struct Foo
    {
      BOOST_HANA_DEFINE_STRUCT(Foo,
                               (std::uint8_t, a),
                               (std::uint16_t, b),
                               (std::uint32_t, c),
                               (std::uint64_t, d),
                               (Bar, bar));
    };

    std::size_t s = alignedsize<Foo>();
    expect(7_ul == s);
  };

  "alignedsize"_test = [&] {
    struct Bar
    {
      BOOST_HANA_DEFINE_STRUCT(Bar,
                               (std::uint8_t, a),
                               (std::uint16_t, b),
                               (std::string_view, s));
    };

    struct Foo
    {
      BOOST_HANA_DEFINE_STRUCT(Foo,
                               (std::uint8_t, a),
                               (std::uint16_t, b),
                               (std::uint32_t, c),
                               (std::uint64_t, d),
                               (Bar, bar));
    };

    Foo foo{ 1, 2, 3, 4, { 5, 6, "haha" } };
    size_t sizeIn = serialize(foo, buffer.begin());
    expect(sizeIn == (alignedsize(foo) +
                      1)); // Length of string is not part of alignedsize!

    expect(alignedsize<TripleFloat32>() == 3);
    expect(alignedsize<TripleFloat16>() == 2);
    TripleFloat16 tmp{ 11, 12, 13 };
    expect(alignedsize<TripleFloat16>(tmp) == 2);
  };

  "alignedsize of XrtHeader"_test = [] {
    page::XrtHeader header;

    expect(alignedsize<page::XrtHeader>() == 10_ul);
    expect(alignedsize<page::XrtHeader>() == alignedsize(header));

    expect(alignedsize<page::HeaderCode>() + alignedsize<page::XrtHeader>() ==
           11_ul);
  };

  "alignedsize of Variable<>"_test = [] {
    Variable<> v{
      1,          "dasda", { 0, 10 },     getTypeCode<std::int32_t>(TypesMap),
      Rate::fast, Unit{},  "asdasdasdasd"
    };

    expect(alignedsize(v) == 23_ul);
  };

  "alignedsize vs serialization size"_test = [] {
    using T = std::array<std::uint32_t, 5>;
    T in{ 11, 12, 13, 14, 15 };
    std::size_t s = alignedsize(in);
    expect(s == alignedsize<T>());
    T out;
    std::array<std::uint32_t, 10> buffer;
    auto in_size = serialize(in, buffer.begin());
    expect(5_ul == in_size);
    expect(s == in_size);
    auto out_size = deserialize(buffer.cbegin(), out);
    expect(5_ul == out_size);
    expect(in == out);
  };

  "alignedsize of DateTime"_test = [] {
    expect(alignedsize<DateTime>() == 1_ul);
  };

  "alignedsize of Timestamp"_test = [] {
    expect(alignedsize<incident::Timestamp>() == 2_ul);
  };
}
