#include <boost/ut.hpp>
#include <cstdint>
#include <cstring>
#include <string_view>
#include "common/page.hpp"
#include "common/incident.hpp"
#include <iterator>
#include "common/helper.hpp"

int main()
{
  using namespace boost::ut;
  using namespace mecos::common; 

  page::Data buffer{};

  "serialize data header"_test = [&buffer]
  {
    page::Entity<page::DataHeader> page{buffer};
    page::DataHeader header{1, 33,{{21,22,23,24,5,26},11},crc32{99}};

    expect(header.sizeOfFirstSample == 33);
    expect(header.dateAndTime.CycleTime == 11);
    expect(to_integer<int>(header.crcOfXrt) == 99);

    std::size_t offset_in = page.serialize(header);

    page::DataHeader h;

    std::size_t offset_out = page.deserialize(h);

    expect(offset_out == offset_in);
    expect(compare(header, h));
    expect(h.sizeOfFirstSample == 33);
    expect(h.dateAndTime.CycleTime == 11);
    expect(to_integer<int>(h.crcOfXrt) == 99);
  };

  "serialize xrt header"_test = [&buffer]
  {
    page::Entity<page::XrtHeader> page{buffer};
    page::XrtHeader header{{'F','W','X','Y','Z'},12,49,{{21,22,23,24,5,26},11},crc32{99}};
    std::size_t offset_in = page.serialize(header);
    expect(header.nVarsPage == 12);
    expect(header.serialNumber == std::array<char, 5>{'F','W','X','Y','Z'});
    page::XrtHeader h;
    std::size_t offset_out = page.deserialize(h);
    expect(offset_in == page.header_offset) << offset_in;
    expect(offset_out == offset_in);
    expect(compare(header, h));
    expect(h.nVarsPage == 12);
    expect(h.serialNumber == std::array<char, 5>{'F','W','X','Y','Z'});
  };

  "serialize init header"_test = [&buffer]
  {
    page::Entity<page::InitHeader> page{buffer};
    page::InitHeader header{0, 22,{{1,2,3,4,5,6}, 99}, crc32{99}};
    expect(to_integer<int>(header.crcOfThisPage) == 99);
    expect(header.nInitPage ==22);
    std::size_t offset_in = page.serialize(header);

    page::InitHeader h;
    std::size_t offset_out = page.deserialize(h);
    expect(offset_out == offset_in);
    expect(compare(header, h));
    expect(to_integer<int>(h.crcOfThisPage) == 99);
    expect(h.nInitPage ==22);
  };

  "getHeaderCode"_test = [&buffer]
  {
    page::Entity<page::InitHeader> page{buffer};
    page::InitHeader header{0, 22,{{1,2,3,4,5,6}, 99}, crc32{99}};
    page.serialize(header);
    auto headerCode = page::getHeaderCode(buffer.begin());
    expect(headerCode == page::HeaderCode::init);

    page::Entity<page::XrtHeader> page2{buffer};
    page::XrtHeader header2{{'F','W','X','Y','Z'},12,49,{{21,22,23,24,5,26},11},crc32{99}};
    page2.serialize(header2);
    headerCode = page::getHeaderCode(buffer.begin());
    expect(headerCode == page::HeaderCode::xrt);

    page::Entity<page::DataHeader> page3{buffer};
    page::DataHeader header3{0,33,{{21,22,23,24,5,26},11},crc32{99}};
    page3.serialize(header3);
    headerCode = page::getHeaderCode(buffer.begin());
    expect(headerCode == page::HeaderCode::data);
  };

  "serialize SinGenParameters"_test = [&buffer] {
    page::Entity<page::DataHeader> page{ buffer };
    page.advanceWriteItToHeaderPos();
    page.advanceReadItToHeaderPos();
    SinGenParameters in;
    fill(in, 123);
    SinGenParameters out;
    page.serialize(in);
    if (page.hasWriteSpace<SinGenParameters>())
      page.serialize(in);
    if (page.hasReadSpace<SinGenParameters>())
      page.deserialize(out);
    expect(compare(in, out));
  };

  "serialize SinGenParameters with VarIndex"_test = [&buffer] {
    page::Entity<page::DataHeader> page{ buffer };
    //Index, LeaseTime, AmplitudeCos, AmplitudeSin
    VarIndex_t varIdxIn = 44;
    VarIndex_t varIdxOut;
    SinGenParameters in{33,20,5,3};
    SinGenParameters out;
    size_t itIn = 0;
    page.advanceWriteItToHeaderPos();
    page.advanceReadItToHeaderPos();
    while (page.hasWriteSpace(varIdxIn, in))
    {
      page.serialize(varIdxIn, in);
      itIn++;
    }
    size_t itOut = 0;
    while (page.hasReadSpace(varIdxOut, out))
    {
      page.deserialize(varIdxOut, out);
      itOut++;
    }
    expect(compare(in, out));
    expect(itIn == itOut);
    expect(varIdxIn == varIdxOut);
  };

  "serialize GeneratorParameters"_test = [&buffer] {
    page::Entity<page::DataHeader> page{ buffer };
    page.advanceWriteItToHeaderPos();
    page.advanceReadItToHeaderPos();
    GeneratorParameters in;
    fill(in, 123);
    GeneratorParameters out;
    page.serialize(in);
    if (page.hasWriteSpace<GeneratorParameters>())
      page.serialize(in);
    if (page.hasReadSpace<GeneratorParameters>())
      page.deserialize(out);
    expect(compare(in, out));
  };
}
