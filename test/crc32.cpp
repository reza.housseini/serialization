#include "common/crc32.hpp"
#include <boost/ut.hpp>

int
main()
{
  using namespace boost::ut;
  using namespace mecos::common;

  "crc32 cast to integer"_test = [] {
    crc32 crc{ 234 };
    expect(234_i == to_integer<int>(crc));
  };

  "crc32 standard calculation"_test = [] {
    std::array<int, 234> a{ 5 };
    auto v = crc32_calc(a.data(), a.size());
    expect(v == crc32{ 0xCA4044BD });
  };

  "crc32 forward syndrom calculation"_test = [] {
    std::array<int, 234> a{ 5 };
    auto v = ecc_gen_fwd_syn32(a.data(), a.size());
    expect(v == crc32{ 0xFEDD5CA1 });
  };
}
