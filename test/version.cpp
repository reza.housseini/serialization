#include <boost/ut.hpp>
#include <common_version.hpp>

int
main()
{
  using namespace boost::ut;

  skip | "versioning"_test = [] {
    expect(0_i == mecos::common::version.patch);
    expect(0_i == mecos::common::version.minor);
    expect(1_i == mecos::common::version.major);
  };
}
