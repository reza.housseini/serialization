#include <boost/ut.hpp>
#include <common/toml.hpp>
using namespace std::string_view_literals;

int
main()
{
  using namespace boost::ut;
  using namespace mecos::common;

  static constexpr auto source = R"(
      a = 1
      b = 0.5
      
      "RPC/addLog" = {VariableIndex = 3, SamplingTime = 5}
      time = {Sec = 1, Min = 2, Hour = 3, Day = 4, Month = 5, Year = 6}
      time_incomplete = {Sec = 1, Min = 2, Hour = 3, Day = 4, Month = 5}
      timestamp = {DateAndTime = {Sec = 1, Min = 2, Hour = 3, Day = 4, Month = 5, Year = 6}, CycleTime = 99}
      c = [1, 2, 3]
    )"sv;

  auto result = toml::parse(source);

#ifdef _MSC_VER
  toml::table table = result;
#else
  toml::table table = result.table();
#endif

  "extract int"_test = [&] {
    int v;
    extract(v, *table.get("a"));
    expect(1_i == v);
  };

  "extract float"_test = [&] {
    float v;
    extract(v, *table.get("b"));
    expect(0.5_d == v);
  };

  "extract LogdataSpecification"_test = [&] {
    LogdataSpecification spec;
    extract(spec, *table.get("RPC/addLog"));
    expect(3_i == spec.VariableIndex);
    expect(5_u == spec.SamplingTime);
  };

  "extract DateTime"_test = [&] {
    DateTime t;
    extract(t, *table.get("time"));
    expect(1_u == t.time.Sec);
    expect(2_u == t.time.Min);
    expect(3_u == t.time.Hour);
    expect(4_u == t.time.Day);
    expect(5_u == t.time.Month);
    expect(6_u == t.time.Year);
  };

  "extract DateTime incomplete"_test = [&] {
    DateTime t;
    extract(t, *table.get("time_incomplete"));
    expect(1_u == t.time.Sec);
    expect(2_u == t.time.Min);
    expect(3_u == t.time.Hour);
    expect(4_u == t.time.Day);
    expect(5_u == t.time.Month);
    expect(0_u == t.time.Year);
  };

  "extract Timestamp"_test = [&] {
    incident::Timestamp t;
    extract(t, *table.get("timestamp"));
    expect(1_u == t.DateAndTime.time.Sec);
    expect(2_u == t.DateAndTime.time.Min);
    expect(3_u == t.DateAndTime.time.Hour);
    expect(4_u == t.DateAndTime.time.Day);
    expect(5_u == t.DateAndTime.time.Month);
    expect(6_u == t.DateAndTime.time.Year);
    expect(99_u == t.CycleTime);
  };

  "extract array"_test = [&] {
    std::array<std::uint32_t, 3> a;
    extract(a, *table.get("c"));
    expect(1_u == a[0]);
    expect(2_u == a[1]);
    expect(3_u == a[2]);
  };

  "extract by type code"_test = [&] {
    auto a = extract_by_type_code(0xcd, *table.get("a"));
    expect(std::get<std::uint16_t>(a) == 1);

    auto b = extract_by_type_code(0xca, *table.get("b"));
    expect(std::get<float>(b) == 0.5_d);
  };
}
