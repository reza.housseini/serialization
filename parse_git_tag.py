#!/usr/bin/python3

import subprocess
import re

out = subprocess.run(["git", "describe", "--abbrev=0", "--tags"], stdout=subprocess.PIPE, encoding='utf-8')

match = re.match(r'^v(?P<major>\d+)\.?(?P<minor>\d+)\.?(?P<patch>.*?|\d+)$', out.stdout)

if match:
    major = match.group('major')
    minor = match.group('minor')
    if match.group('patch'):
        patch = match.group('patch')
    else:
        patch = 0
else:
    major = 0
    minor = 0
    patch = 0

print("{} {} {}".format(major, minor, patch))
