#include <boost/hana.hpp>
#include <cstdint>

namespace mecos
{
namespace common
{

struct Version
{ // 	see https://semver.org/lang/de/
  BOOST_HANA_DEFINE_STRUCT(Version,
                           (std::uint8_t, patch), // compatible bugfixes
                           (std::uint8_t,
                            minor), // new functions, backward compatible
                           (std::uint8_t, major),     // incompatible
                           (std::uint8_t, reserved)); //
};

} // namespace common
} // namespace mecos
