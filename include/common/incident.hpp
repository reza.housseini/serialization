#pragma once

#include <array>
#include <boost/hana.hpp>
#include <cstddef>
#include <cstdint>
#include <cstring>

namespace mecos
{
namespace common
{

static const std::uint32_t YearOffset = 2020;

// ToDo: Bue: Gehört dies alles  zum common ?? Ich dachte,  "struct Time" gehört
// ins variable.hpp, der Rest zur firmware
struct Time
{
  std::uint32_t Sec : 6;
  std::uint32_t Min : 6;
  std::uint32_t Hour : 5;
  std::uint32_t Day : 5;
  std::uint32_t Month : 4;
  std::uint32_t Year : 6;
}
#ifndef _MSC_VER
__attribute__((packed))
#endif
;

union DateTime
{
  Time time;
  std::uint32_t uint32;
};

namespace hana = boost::hana;

enum class LogType : std::uint8_t
{
  NoLog,
  Snapshot_only, // rate=0: Do StateUpload only
  Decimate, // rate=1: additionally IncidentDecimationRate (usually 1Hz), during
            // DataLogCountBack ticks
  LogSlow,  // rate=2: additionally safe subsLogSlowBuffer (usually 100Hz),
            // DataLogSizeSlow pages
  Log10k,   // rate=3: additionally safe subsLogFastBuffer2 (usually 10kHz),
            // DataLogSize10k pages
  Log20k,   // rate=4: additionally safe subsLogFastBuffer0 (usually 20kHz),
            // DataLogSize20k pages
  Log40k,   // rate=5: additionally safe subsLogFastBuffer1&3 (usually 40kHz),
            // DataLogSize40k pages
  Count
};

namespace incident
{

using CycleTime = std::uint32_t;
using FilterValue = std::uint32_t;
using Counter = std::uint32_t;
using LimitCounter = volatile Counter;

using ID = std::uint16_t;
using ExtraData = std::array<std::uint32_t, 4>;

struct Timestamp
{
  BOOST_HANA_DEFINE_STRUCT(Timestamp,
                           (DateTime, DateAndTime),
                           (std::uint32_t, CycleTime));
};

inline bool
operator==(const Timestamp& timestamp1, const Timestamp& timestamp2)
{
  return timestamp1.CycleTime == timestamp2.CycleTime &&
         timestamp1.DateAndTime.uint32 == timestamp2.DateAndTime.uint32;
}

struct LogDefinition
{
  BOOST_HANA_DEFINE_STRUCT(LogDefinition,
                           (ID, id),
                           (Timestamp, time),
                           (ExtraData, attributes));
};

enum class Resetable : std::uint8_t
{
  Always,
  InStandstill,        // Rotaing Bit  ModelStatesd
  AfterConverterReset, // PWM_123      External_outputs
  Never,
  Count
};

enum class Levels : std::uint8_t
{
  NoThreat,
  Information,
  Warning,
  Fault,
  Count
};

enum class Reactions : std::uint8_t
{
  NoReaction,
  LogOnly,
  Stop,
  DisableIverter, // entspricht Stop
  DisableRectifier,
  DisableConverter, // entspricht Gleichrichter aus
  Count
};

/*
 * Limits can be defined as float or as Int32 or unsigned int32
 * for 16 or 8Bit values the limits are casted during limit check
 */
union Limit
{
  std::uint32_t AsUint;
  std::int32_t AsInt;
  float AsFloat;
};

struct DefinitionData
{
  BOOST_HANA_DEFINE_STRUCT(DefinitionData,
                           (ID, Id),
                           (Levels, TheLevel),
                           (Reactions, Reaction),
                           (Resetable, IsResettable),
                           (LogType, LogsIn));
};

template<typename StringType = std::string_view>
struct Definition
{
  BOOST_HANA_DEFINE_STRUCT(Definition,
                           (incident::DefinitionData, Data),
                           (StringType, Name),
                           (StringType, Description));
};

struct LimitData
{
  // Limit of the measured value at which the error is triggered
  Limit* Value;
  // error is only triggered when FilterCount >= FilterLimit
  FilterValue* Filter;
  // Number of successive limit value exceedances
  LimitCounter* IncidentCount;
};

struct ControlData
{
  LimitData Limits;
  const DefinitionData*
    Error; /* Reference/Pointer to table in ErrorHandler.hpp */
  Timestamp IncidentTime;
  ExtraData Attributes;
};

/*
 * We can check for upper and lower limit violations
 */
enum class LimitLevels : std::uint8_t
{
  Lower,
  Upper,
  Count
};

struct FindSpecification // ToDo  Bue: I think this should be replaced, see
                         // below
{
  BOOST_HANA_DEFINE_STRUCT(FindSpecification,
                           (Timestamp, TimesOfIncident),
                           (Levels, LevelOfIncident),
                           (std::int16_t, NumberOfIncidents));
};
//
// struct FindSpecification // this is used to find the IncidentCounter from a
// specific DateAndTime)
//{
//  BOOST_HANA_DEFINE_STRUCT(FindSpecification,
//                           (DateTime, DateAndTime),
//                           (Levels, LevelOfIncident),
//						   (uint32, IncidentCounter));
//// IncidentCounter not used in RPC/FindIncident, But used as output argument
//}

struct ReadSpecification
{
  BOOST_HANA_DEFINE_STRUCT(
    ReadSpecification,
    (std::int32_t, IncidentCounter),
    // Use negative values to count from top (-1 = last incident)
    (Levels, LevelOfIncident),
    (std::int16_t, NumberOfIncidents));
};

} // namespace incident
} // namespace common
} // namespace mecos
