#pragma once
#include <algorithm>
#include <array>
#include <iostream>
#include <optional>
#include <regex>
#include <string>

namespace mecos
{
namespace common
{

class Serialnumber
{
public:
  using value_type = std::array<char, 5>;

  Serialnumber() {}

  static std::optional<Serialnumber> construct(const std::string& serialnumber)
  {
    if (is_valid(serialnumber))
    {
      value_type sn;
      std::copy(serialnumber.begin(), serialnumber.end(), sn.begin());
      return Serialnumber(sn);
    }

    return std::nullopt;
  }

  static std::optional<Serialnumber> construct(const value_type& serialnumber)
  {
    if (is_valid(serialnumber))
      return Serialnumber(serialnumber);

    return std::nullopt;
  }

  std::string to_string() const
  {
    return std::string(value.begin(), value.end());
  }

private:
  Serialnumber(const value_type& serialnumber)
    : value(serialnumber)
  {
  }

  static bool is_valid(const std::string& serialnumber)
  {
    return std::regex_match(serialnumber, std::regex{ "[A-Za-z]{5}" });
  }

  static bool is_valid(const value_type& value)
  {
    return std::regex_match(std::string(value.begin(), value.end()),
                            std::regex{ "[A-Za-z]{5}" });
  }

  value_type value;
};

} // namespace common
} // namespace mecos
