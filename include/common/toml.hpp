#pragma once
#include "common/types.hpp"
#include <boost/hana.hpp>
#include <toml++/toml.h>
#include <utility>

namespace common = mecos::common;
namespace hana = boost::hana;

namespace mecos
{
namespace common
{

template<typename T>
void
extract(T& t, const toml::node& node)
{
  if constexpr (std::is_same<T, DateTime>::value)
  {
    auto table = node.as_table();
    int v;
    if (table->contains("Sec"))
    {
      extract(v, *table->get("Sec"));
      t.time.Sec = v;
    }
    else
      t.time.Sec = 0;
    if (table->contains("Min"))
    {
      extract(v, *table->get("Min"));
      t.time.Min = v;
    }
    else
      t.time.Min = 0;
    if (table->contains("Hour"))
    {
      extract(v, *table->get("Hour"));
      t.time.Hour = v;
    }
    else
      t.time.Hour = 0;
    if (table->contains("Day"))
    {
      extract(v, *table->get("Day"));
      t.time.Day = v;
    }
    else
      t.time.Day = 0;
    if (table->contains("Month"))
    {
      extract(v, *table->get("Month"));
      t.time.Month = v;
    }
    else
      t.time.Month = 0;
    if (table->contains("Year"))
    {
      extract(v, *table->get("Year"));
      t.time.Year = v;
    }
    else
      t.time.Year = 0;
  }
  else if constexpr (std::is_same<T, TripleFloat16>::value)
  {
    extract(t.float32, node);
  }
  else if constexpr (std::is_integral<T>::value)
  {
    auto v = node.value<int>();
    if (v)
      t = T{ static_cast<T>(*v) };
    else
      t = T{};
  }
  else if constexpr (std::is_enum<T>::value)
  {
    auto v = node.value<int>();
    if (v)
      t = T{ static_cast<std::underlying_type_t<T>>(*v) };
    else
      t = T{};
  }
  else if constexpr (std::is_floating_point<T>::value)
  {
    auto v = node.value<float>();
    if (v)
      t = T{ *v };
    else
      t = T{};
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    auto array = node.as_array();
    auto counter = array->cbegin();
    for (auto& item : t)
    {
      if (counter != array->cend())
        extract(item, *counter);
      else
        break;
      ++counter;
    }
  }
  else if constexpr (std::is_class<T>::value)
  {
    auto table = node.as_table();
    hana::for_each(hana::accessors<T>(), [&](auto&& accessor) {
      auto key = hana::to<char const*>(hana::first(accessor));
      auto get_member = hana::second(accessor);
      if (table->contains(key))
      {
        extract(get_member(t), *table->get(key));
      }
    });
  }
};

inline DataTypes
extract_by_type_code(std::uint8_t type_code, const toml::node& node)
{
  DataTypes value;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (type_code == code)
                   {
                     using T = typename decltype(type)::type;
                     auto t = T();
                     extract(t, node);
                     value = t;
                   };
                 }));
  return value;
};

} // common
} // mecos
