#pragma once
#include <array>

namespace mecos
{
namespace common
{

/**
 * DYNAMIC LIST CLASS
 * Allocates the whole memory (maxSize) but can be used as a dynamic list.
 * The data is saved in a std::array (dataArray) and the actually used size of
 * the arrayis saved in the property currentSize. If an element from within the
 * array is erased, the last element is copied to the slot within the array.
 * This is to ensure that the unused slots are always at the end of the array.
 * A second std::array (doubleLinkedArray) of struct doubleLinkedNode with same
 * length as dataArray is used to determine the order of the data. Index 'x' of
 * doubleLInkedArray contains the indices of of the following and previous
 * data-indices of index 'x' in the dataArray.
 * */
template<typename T, std::size_t maxSize>
class List : private std::array<T, maxSize>
{

private:
  typedef struct
  {
    std::size_t prev;
    std::size_t next;
  } DoubleLinkedNode;
  typedef std::array<T, maxSize>
    dataArray; /**< Parent std::array which contains the actual data */
  std::size_t currentSize; /**< gives the current size of the dynamic list */
  std::array<DoubleLinkedNode, maxSize>
    doubleLinkedArray;   /** linked array to know the order of the elements */
  std::size_t startNode; /** first elements index */

  /** Private function to find the position of an index in 'list' in the
   * pre-allocated std::array */
  std::size_t findArrayIndex(std::size_t index) const
  {
    bool reversed = false;
    std::size_t iterator = 0;
    std::size_t elementI = startNode;

    // Search from the back if index is more than half of the size (performance)
    if (index - 1 > currentSize / 2)
    {
      reversed = true;
      index = currentSize - index;
    }

    while (iterator < index)
    {
      if (reversed)
      {
        elementI = doubleLinkedArray[elementI].prev;
      }
      else
      {
        elementI = doubleLinkedArray[elementI].next;
      }
      iterator += 1;
    }
    return elementI;
  }

public:
  /** Constructor */
  List() { clear(); }

  /** Inherited max_size() function of Parent class */
  using dataArray::max_size;
  /** Get the current used size of the pre-allocated std::array */
  std::size_t size() const { return currentSize; }

  /** Delete the whole content of the Instance */
  void clear()
  {
    currentSize = 0;
    startNode = 0;
    doubleLinkedArray[0].prev = 0;
    doubleLinkedArray[0].next = 0;
  }

  /** Access the first element of the list */
  T front() const { return dataArray::operator[](startNode); }

  /** Access the last element of the list */
  T back() const
  {
    return dataArray::operator[](doubleLinkedArray[startNode].prev);
  }

  /** Adds an element to the front */
  void pushFront(T element)
  {
    // Add element to dataArray
    dataArray::operator[](currentSize) = element;

    // Add new element links to doubleLinkedArray
    doubleLinkedArray[currentSize].next = startNode;
    doubleLinkedArray[currentSize].prev = doubleLinkedArray[startNode].prev;

    // Update elements of doubleLinkedArray before and after this new element
    doubleLinkedArray[doubleLinkedArray[startNode].prev].next = currentSize;
    doubleLinkedArray[startNode].prev = currentSize;

    // Update size and startNode to new values
    startNode = currentSize;
    currentSize += 1;
  }

  /** Adds an element to the end */
  void pushBack(T element)
  {
    // Add element to dataArray
    dataArray::operator[](currentSize) = element;

    // Add new element links to doubleLinkedArray
    doubleLinkedArray[currentSize].next = startNode;
    doubleLinkedArray[currentSize].prev = doubleLinkedArray[startNode].prev;

    // Update elements of doubleLinkedArray before and after this new element
    doubleLinkedArray[doubleLinkedArray[startNode].prev].next = currentSize;
    doubleLinkedArray[startNode].prev = currentSize;

    // Update size to new value
    currentSize += 1;
  }

  /** Remove first element of instance-list */
  void popFront() { erase(0); }
  /** Remove last element of instance-list */
  void popBack() { erase(currentSize - 1); }

  /** Insert element before index.
   * index can be this->currentSize. In this case a pushBack() is executed.
   */
  void insert(T element, std::size_t index)
  {
    if (index == currentSize)
    {
      pushBack(element);
    }
    else
    {
      // Insert data to dataArray and initialize doubleLinkedArray
      std::size_t insertIndex = findArrayIndex(index);
      dataArray::operator[](currentSize) = element;
      doubleLinkedArray[currentSize].prev = doubleLinkedArray[insertIndex].prev;
      doubleLinkedArray[currentSize].next = insertIndex;

      // Update slots surrounding the inserted element
      doubleLinkedArray[doubleLinkedArray[insertIndex].prev].next = currentSize;
      doubleLinkedArray[insertIndex].prev = currentSize;

      // Check if startNode is still valid
      if (index == 0)
      {
        startNode = currentSize;
      }

      // Increment current size
      currentSize += 1;
    }
  }

  /** Delete element with index */
  void erase(std::size_t index)
  {
    // Get index of element in dataArray
    std::size_t elementI = findArrayIndex(index);

    // Update elements of doubleLinkedArray
    doubleLinkedArray[doubleLinkedArray[elementI].next].prev =
      doubleLinkedArray[elementI].prev;
    doubleLinkedArray[doubleLinkedArray[elementI].prev].next =
      doubleLinkedArray[elementI].next;

    // Adjust arrays so that empty slots are always in the end of the array!
    // Attention when startNode is somehow affected!
    if (startNode == elementI)
    {
      startNode = doubleLinkedArray[startNode].next;
    }
    if (elementI != currentSize - 1)
    {
      dataArray::operator[](elementI) = dataArray::operator[](currentSize - 1);
      doubleLinkedArray[elementI].next =
        doubleLinkedArray[currentSize - 1].next;
      doubleLinkedArray[elementI].prev =
        doubleLinkedArray[currentSize - 1].prev;
      doubleLinkedArray[doubleLinkedArray[elementI].next].prev = elementI;
      doubleLinkedArray[doubleLinkedArray[elementI].prev].next = elementI;
      if (startNode == currentSize - 1)
      {
        startNode = elementI;
      }
    }

    // Decrement size
    currentSize -= 1;
  }

  /** [] Operater to access index of list in instance */
  T operator[](std::size_t accessIndex) const
  {
    // generate error if index > size-1
    return dataArray::operator[](findArrayIndex(accessIndex));
  }
};

} /* namespace common */
} /* namespace mecos */
