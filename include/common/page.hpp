#pragma once
#include "common/serialization.hpp"
#include "common/types.hpp"
#include <boost/hana.hpp>
#include <cstdint>

namespace hana = boost::hana;

namespace mecos
{
namespace common
{
namespace page
{

enum class HeaderCode : std::uint8_t
{
  xrt,
  data,
  incident,
  init
};

struct DataHeader
{
  BOOST_HANA_DEFINE_STRUCT(DataHeader,
                           (std::uint8_t, isLog),
                           (std::uint16_t, sizeOfFirstSample),
                           (incident::Timestamp, dateAndTime),
                           (crc32, crcOfXrt));
};

struct XrtHeader
{
  BOOST_HANA_DEFINE_STRUCT(XrtHeader,
                           (std::array<char, 5>, serialNumber),
                           (std::uint16_t,
                            nVarsPage), // Number of variables in this page
                           (std::uint16_t,
                            nVarsXrt), // Number of variables in whole XRT
                           (incident::Timestamp, dateAndTime),
                           (crc32, crcOfXrt));
};

struct IncidentHeader
{
  BOOST_HANA_DEFINE_STRUCT(
    IncidentHeader,
    (std::array<char, 5>, serialNumber),
    (std::uint16_t,
     nEntriesPage), // Number of variables in this page
    (std::uint16_t,
     nEntriesTable), // Number of variables in whole Table
    (incident::Timestamp, dateAndTime),
    (crc32, crcOfIncidentTable));
};

struct InitHeader
{
  BOOST_HANA_DEFINE_STRUCT(InitHeader,
                           (std::uint8_t, dummy),
                           (std::uint16_t,
                            nInitPage), // Number of this Init-page
                           (incident::Timestamp, dateAndTime),
                           (crc32, crcOfThisPage)); // CRC32 of this page
};

//! Mapping between header type and header type code as a single source of
//! truth
constexpr auto HeaderTypesMap = hana::make_tuple(
  hana::make_pair(hana::type_c<XrtHeader>,
                  hana::integral_c<HeaderCode, HeaderCode::xrt>),
  hana::make_pair(hana::type_c<DataHeader>,
                  hana::integral_c<HeaderCode, HeaderCode::data>),
  hana::make_pair(hana::type_c<IncidentHeader>,
                  hana::integral_c<HeaderCode, HeaderCode::incident>),
  hana::make_pair(hana::type_c<InitHeader>,
                  hana::integral_c<HeaderCode, HeaderCode::init>));

enum class Mode : uint8_t
{
  ready,
  writing,
  full,
  sending
};

// data structure of a page

static const uint32_t size = 1024;
struct Data : public std::array<DataBase_t, size>
{
  Data()
    : writeit(begin())
    , readit(cbegin())
    , mode(Mode::ready)
  {
  }

  Data::iterator writeit;      // iterator write position
  Data::const_iterator readit; // iterator read position
  Mode mode;
};

template<typename Header>
class Entity
{
public:
  Entity(Data& data)
    : data_(data)
  {
  }

  inline void advanceReadItToHeaderPos()
  {
    data_.readit = std::next(data_.cbegin(), header_offset);
  }

  inline void advanceWriteItToHeaderPos()
  {
    data_.writeit = std::next(data_.begin(), header_offset);
  }

  inline std::size_t serialize(const Header& header, bool advance = true)
  {
    std::size_t offset = common::serialize(code, data_.begin());
    offset += common::serialize(header, std::next(data_.begin(), offset));
    if (advance)
      data_.writeit = std::next(data_.begin(), header_offset);
    return offset;
  }

  inline std::size_t deserialize(Header& header, bool advance = true)
  {
    HeaderCode code_tmp;
    std::size_t offset = common::deserialize(data_.cbegin(), code_tmp);
    if (code_tmp != code)
      return 0;
    offset += common::deserialize(std::next(data_.cbegin(), offset), header);
    if (advance)
      data_.readit = std::next(data_.cbegin(), header_offset);
    return offset;
  }

  template<typename T>
  inline std::size_t serialize(const T& t)
  {
    std::size_t offset = common::serialize(t, data_.writeit);
    std::advance(data_.writeit, offset);
    return offset;
  }

  template<typename T>
  inline std::size_t deserialize(T& t)
  {
    std::size_t offset = common::deserialize(data_.readit, t);
    std::advance(data_.readit, offset);
    return offset;
  }

  template<typename T>
  inline std::size_t serialize(const VarIndex_t& varIndex, const T& t)
  {
    std::size_t offset = common::serialize(varIndex, t, data_.writeit);
    std::advance(data_.writeit, offset);
    return offset;
  }

  template<typename T>
  inline std::size_t deserialize(VarIndex_t& varIndex, T& t)
  {
    std::size_t offset = common::deserialize(data_.readit, varIndex, t);
    std::advance(data_.readit, offset);
    return offset;
  }

  template<typename T>
  inline bool hasWriteSpace()
  {
    auto position = std::distance(data_.begin(), data_.writeit);
    return (position + common::alignedsize<T>()) < size;
  }

  template<typename T>
  inline bool hasWriteSpace(const T& t)
  {
    auto position = std::distance(data_.begin(), data_.writeit);
    return (position + common::alignedsize(t)) < size;
  }

  template<typename T>
  inline bool hasWriteSpace(const VarIndex_t& /*varIndex*/, const T& t)
  {
    auto position = std::distance(data_.begin(), data_.writeit);
    return (position + common::alignedsize<VarIndex_t>() +
            common::alignedsize(t)) < size;
  }

  template<typename T>
  inline bool hasReadSpace()
  {
    auto position = std::distance(data_.cbegin(), data_.readit);
    return (position + common::alignedsize<T>()) < size;
  }

  template<typename T>
  inline bool hasReadSpace(const T& t)
  {
    auto position = std::distance(data_.cbegin(), data_.readit);
    return (position + common::alignedsize(t)) < size;
  }

  template<typename T>
  inline bool hasReadSpace(const VarIndex_t& /*varIndex*/, const T& t)
  {
    auto position = std::distance(data_.cbegin(), data_.readit);
    return (position + common::alignedsize<VarIndex_t>() +
            common::alignedsize(t)) < size;
  }

  inline std::size_t getWriteSpace()
  {
    return std::distance(data_.writeit, data_.end());
  };

  inline std::size_t getReadSpace()
  {
    return std::distance(data_.readit, data_.cend());
  };

  inline Data::iterator getWriteIterator() { return data_.writeit; };

  inline Data::const_iterator getReadIterator() { return data_.readit; };

  inline void advanceReadIterator(size_t offset)
  {
    std::advance(data_.readit, offset);
  };

  inline void advanceWriteIterator(size_t offset)
  {
    std::advance(data_.writeit, offset);
  };

public:
  static constexpr HeaderCode code = getTypeCode<Header>(HeaderTypesMap);
  static constexpr std::size_t header_offset =
    common::alignedsize<HeaderCode>() + common::alignedsize<Header>();

private:
  Data& data_;
};

inline HeaderCode
getHeaderCode(Data::const_iterator buffer)
{
  HeaderCode code;
  common::deserialize(buffer, code);
  return code;
}

} // namespace page

struct send_buffer : public page::Data
{
};

struct receive_buffer : public page::Data
{
};

} // namespace common
} // namespace mecos
