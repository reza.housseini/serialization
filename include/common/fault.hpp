#pragma once
#include <string_view>
#include <array>

namespace mecos { namespace common {

struct fault
{
  const std::size_t index;
  const std::string_view name;
  const std::string_view info;
};

template <size_t N>
using fault_table_t = std::array<fault, N>;

/*
const size_t num_faults = 2;
fault_table_t<num_faults> fault_table = {
  {0, "", ""},
  {1, "", ""}
};
*/

} /* namespace common */ } /* namespace mecos */
