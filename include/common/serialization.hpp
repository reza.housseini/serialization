#pragma once

#include "common/size.hpp"
#include "common/types.hpp"
#include "common/variable.hpp"
#include <algorithm>
#include <array>
#include <boost/hana.hpp>
#include <cstddef>
#include <cstdint>
#include <cstring>
#include <string_view>
#include <tuple>
#include <type_traits>
#include <utility>
#include <vector>

namespace mecos
{
namespace common
{

namespace hana = boost::hana;

#if defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_BIG_ENDIAN__
static const bool isLittleEndianHost = false;
#elif defined(__BYTE_ORDER__) && __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__
static const bool isLittleEndianHost = true;
#elif defined(_MSC_VER)
static const bool isLittleEndianHost = true;
#else
#error "I don't know what architecture this is!"
#endif

//! Network endianness is set to little endian
static const bool isLittleEndianNetwork = true;

//! Move function with compile time endianness check
template<class InputIterator, class OutputIterator>
inline constexpr void
endianRespectingCopy(InputIterator first,
                     InputIterator last,
                     OutputIterator result)
{
  if constexpr (isLittleEndianHost != isLittleEndianNetwork)
    std::copy_backward(first, last, result);
  else
    std::copy(first, last, result);
}

template<typename T, typename U>
inline U
pack(std::array<T, div_ceil(sizeof(U), sizeof(T))> values)
{
  return *reinterpret_cast<U*>(values.data());
}

//! Forward declared template specialization for std::array<T, N>
template<typename T, std::size_t N, class OutputIterator>
std::size_t
serialize(const std::array<T, N>& value, OutputIterator buffer);

template<typename T, class OutputIterator>
std::size_t
serialize(const T& value, OutputIterator buffer)
{
  if constexpr (std::is_same<T, DateTime>::value)
  {
    return serialize(value.uint32, buffer);
  }
  else if constexpr (std::is_same<T, TripleFloat32>::value)
  {
    size_t offset = serialize(value[0], buffer);
    offset += serialize(value[1], std::next(buffer, offset));
    offset += serialize(value[2], std::next(buffer, offset));
    return offset;
  }
  else if constexpr (std::is_same<T, TripleFloat16>::value)
  {
    std::uint32_t tmp1 =
      pack<std::uint16_t, std::uint32_t>({ value.raw[1], value.raw[3] });
    std::uint32_t tmp2 =
      pack<std::uint16_t, std::uint32_t>({ value.raw[5], 0 });
    std::size_t offset = serialize(tmp1, buffer);
    offset += serialize(tmp2, std::next(buffer, offset));
    return offset;
  }
  else if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
  {
    typedef
      typename std::iterator_traits<OutputIterator>::value_type BufferType;
    constexpr auto alignedTypeSize = alignedsize<T>();
    auto raw = reinterpret_cast<BufferType const*>(&value);
    endianRespectingCopy(raw, std::next(raw, alignedTypeSize), buffer);
    return alignedTypeSize;
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    auto offset = serialize(static_cast<std::uint32_t>(value.size()), buffer);
    for (auto& item : value)
      offset += serialize(item, std::next(buffer, offset));
    return offset;
  }
  else if constexpr (std::is_class<T>::value)
  {
    std::size_t offset = 0;
    hana::for_each(value, hana::fuse([&](auto /*name*/, auto member) {
                     offset += serialize(member, std::next(buffer, offset));
                   }));
    return offset;
  }
}

//! Template specialization for std::array<T, N>
template<typename T, std::size_t N, class OutputIterator>
std::size_t
serialize(const std::array<T, N>& value, OutputIterator buffer)
{
  std::size_t offset = 0;
  for (const auto& item : value)
    offset += serialize(item, std::next(buffer, offset));
  return offset;
}

//! Forward declared template specialization for std::array<T, N>
template<typename T, std::size_t N, class OutputIterator>
std::size_t
serialize(VarIndex_t index,
          const std::array<T, N>& value,
          OutputIterator buffer);

template<typename T, class OutputIterator>
std::size_t
serialize(VarIndex_t index, const T& value, OutputIterator buffer)
{
  if constexpr (std::is_same<T, bool>::value)
  {
    page::DataBase_t tmp = pack<uint16_t, page::DataBase_t>({ index, value });
    return serialize(tmp, buffer);
  }
  else if constexpr (std::is_same<T, uint8_t>::value)
  {
    page::DataBase_t tmp = pack<uint16_t, page::DataBase_t>({ index, value });
    return serialize(tmp, buffer);
  }
  else if constexpr (std::is_same<T, int8_t>::value)
  {
    page::DataBase_t tmp =
      pack<uint16_t, page::DataBase_t>({ index, static_cast<uint16_t>(value) });
    return serialize(tmp, buffer);
  }
  else if constexpr (std::is_same<T, uint16_t>::value)
  {
    page::DataBase_t tmp = pack<uint16_t, page::DataBase_t>({ index, value });
    return serialize(tmp, buffer);
  }
  else if constexpr (std::is_same<T, int16_t>::value)
  {
    page::DataBase_t tmp =
      pack<int16_t, page::DataBase_t>({ static_cast<int16_t>(index), value });
    return serialize(tmp, buffer);
  }
  else if constexpr (std::is_same<T, TripleFloat16>::value)
  {
    std::uint32_t tmp1 =
      pack<std::uint16_t, std::uint32_t>({ index, value.raw[1] });
    std::uint32_t tmp2 =
      pack<std::uint16_t, std::uint32_t>({ value.raw[3], value.raw[5] });
    std::size_t offset = serialize(tmp1, buffer);
    offset += serialize(tmp2, std::next(buffer, offset));
    return offset;
  }
  else if constexpr (std::is_same<T, TripleFloat32>::value)
  {
    size_t offset = serialize(index, buffer);
    offset += serialize(value, std::next(buffer, offset));
    return offset;
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    std::uint32_t tmp = pack<std::uint16_t, std::uint32_t>(
      { index, static_cast<std::uint16_t>(value.size()) });
    auto offset = serialize(tmp, buffer);
    for (auto it = value.begin(); it != value.end(); ++it)
      offset += serialize(*it, std::next(buffer, offset));
    return offset;
  }
  else
  {
    auto offset = serialize(index, buffer);
    offset += serialize(value, std::next(buffer, offset));
    return offset;
  }
}

//! Template specialization for std::array<T, N>
template<typename T, std::size_t N, class OutputIterator>
std::size_t
serialize(VarIndex_t index,
          const std::array<T, N>& value,
          OutputIterator buffer)
{
  auto offset = serialize(index, buffer);
  offset += serialize(value, std::next(buffer, offset));
  return offset;
}

template<class OutputIterator>
auto
serializerFactory(std::uint8_t typeCode, const DataTypes& value)
{
  std::function<std::size_t(OutputIterator)> serializer;
  std::size_t alignedTypeSize;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // add one for the type code
                     alignedTypeSize = alignedsize<T>() + 1;

                     serializer = [value](OutputIterator buffer) {
                       return serialize(std::get<T>(value), buffer);
                     };
                   }
                 }));
  return std::make_pair(serializer, alignedTypeSize);
}

template<class OutputIterator>
auto
serializerFactory(VarIndex_t index,
                  std::uint8_t typeCode,
                  const DataTypesPointer value)
{
  std::function<std::size_t(OutputIterator)> serializer;
  std::size_t alignedTypeSize;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // add one for the type code
                     alignedTypeSize = alignedsize<T>() + 1;

                     serializer = [index, value](OutputIterator buffer) {
                       auto data =
                         std::get<typename std::add_pointer<T>::type>(value);
                       return serialize(index, *data, buffer);
                     };
                   };
                 }));
  return std::make_pair(serializer, alignedTypeSize);
}

template<class OutputIterator>
auto
serializerFactory(VarIndex_t index, std::uint8_t typeCode)
{
  std::function<std::size_t(const DataTypes&, OutputIterator)> serializer;
  std::size_t alignedTypeSize;
  hana::for_each(
    TypesMap, hana::fuse([&](auto type, auto code) {
      if (typeCode == code)
      {
        using T = typename decltype(type)::type;
        // add one for the varindex
        alignedTypeSize = alignedsize<T>() + 1; // This is wrong for sure!

        serializer = [index](const DataTypes& value, OutputIterator buffer) {
          return serialize(index, std::get<T>(value), buffer);
        };
      }
    }));
  return std::make_pair(serializer, alignedTypeSize);
}

template<class OutputIterator>
auto
serializerFactory(VarIndex_t index,
                  std::uint8_t typeCode,
                  const DataTypes& value)
{
  std::function<std::size_t(OutputIterator)> serializer;
  std::size_t alignedTypeSize;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // add one for the type code
                     alignedTypeSize = alignedsize<T>() + 1;

                     serializer = [&, index, value](OutputIterator buffer) {
                       return serialize(index, std::get<T>(value), buffer);
                     };
                   }
                 }));
  return std::make_pair(serializer, alignedTypeSize);
}

template<typename T, typename U>
inline auto
unpack(T t)
{
  constexpr auto length = div_ceil(sizeof(T), sizeof(U));
  auto values = reinterpret_cast<std::array<U, length>*>(&t);
  return std::tuple_cat(*values);
}

//! Forward declared template for std::array<T, N>
template<class InputIterator, typename T, std::size_t N>
std::size_t
deserialize(const InputIterator& buffer, std::array<T, N>& value);

template<class InputIterator, typename T>
std::size_t
deserialize(const InputIterator& buffer, T& value)
{
  if constexpr (std::is_same<T, DateTime>::value)
  {
    return deserialize(buffer, value.uint32);
  }
  else if constexpr (std::is_same<T, TripleFloat16>::value)
  {
    std::uint32_t tmp1, tmp2;
    std::size_t offset = deserialize(buffer, tmp1);
    offset += deserialize(std::next(buffer, offset), tmp2);
    value.raw = std::array<std::uint16_t, 6>{};
    std::tie(value.raw[1], value.raw[3]) =
      unpack<std::uint32_t, std::uint16_t>(tmp1);
    std::tie(value.raw[5], std::ignore) =
      unpack<std::uint32_t, std::uint16_t>(tmp2);
    return offset;
  }
  else if constexpr (std::is_same<T, TripleFloat32>::value)
  {
    size_t offset = deserialize(buffer, value[0]);
    offset += deserialize(std::next(buffer, offset), value[1]);
    offset += deserialize(std::next(buffer, offset), value[2]);
    return offset;
  }
  else if constexpr (std::is_same<T, std::string>::value)
  {
    std::uint32_t length;
    auto offset = deserialize(buffer, length);
    value.resize(length);
    for (auto it = value.begin(); it != std::next(value.begin(), length); ++it)
      offset += deserialize(std::next(buffer, offset), *it);
    return offset;
  }
  else if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
  {
    typedef typename std::iterator_traits<InputIterator>::value_type BufferType;
    constexpr auto bufferTypeSize = sizeof(BufferType);
    constexpr auto alignedTypeSize = div_ceil(sizeof(T), bufferTypeSize);
    if constexpr (bufferTypeSize > sizeof(T))
    {
      auto raw = BufferType{};
      endianRespectingCopy(buffer, std::next(buffer, alignedTypeSize), &raw);
      value = static_cast<T>(raw);
    }
    else
    {
      auto raw = reinterpret_cast<BufferType*>(&value);
      endianRespectingCopy(buffer, std::next(buffer, alignedTypeSize), raw);
    }
    return alignedTypeSize;
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    std::uint32_t length;
    auto offset = deserialize(buffer, length);
    for (auto it = value.begin(); it != std::next(value.begin(), length); ++it)
      offset += deserialize(std::next(buffer, offset), *it);
    return offset;
  }
  else if constexpr (std::is_class<T>::value)
  {
    std::size_t offset = 0;
    hana::for_each(hana::accessors<T>(), [&](auto&& accessor) {
      auto get_member = hana::second(accessor);
      offset += deserialize(std::next(buffer, offset), get_member(value));
    });
    return offset;
  }
}

//! Template specialization for std::array<T, N>
template<class InputIterator, typename T, std::size_t N>
std::size_t
deserialize(const InputIterator& buffer, std::array<T, N>& value)
{
  std::size_t offset = 0;
  for (auto it = value.begin(); it != value.end(); ++it)
    offset += deserialize(std::next(buffer, offset), *it);
  return offset;
}

//! Forward declared template for std::array<T, N>
template<class InputIterator, typename T, std::size_t N>
std::size_t
deserialize(const InputIterator& buffer,
            VarIndex_t& index,
            std::array<T, N>& value);

template<class InputIterator, typename T>
std::size_t
deserialize(const InputIterator& buffer, VarIndex_t& index, T& value)
{
  if constexpr (std::is_same<T, bool>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::tie(index, value) = unpack<std::uint32_t, std::uint16_t>(tmp);
    return offset;
  }
  else if constexpr (std::is_same<T, std::uint8_t>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::uint16_t v;
    std::tie(index, v) = unpack<std::uint32_t, std::uint16_t>(tmp);
    value = static_cast<std::uint8_t>(v);
    return offset;
  }
  else if constexpr (std::is_same<T, std::int8_t>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::uint16_t v;
    std::tie(index, v) = unpack<std::uint32_t, std::uint16_t>(tmp);
    value = static_cast<std::int8_t>(v);
    return offset;
  }
  else if constexpr (std::is_same<T, std::uint16_t>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::tie(index, value) = unpack<std::uint32_t, std::uint16_t>(tmp);
    return offset;
  }
  else if constexpr (std::is_same<T, std::int16_t>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::tie(index, value) = unpack<std::uint32_t, std::uint16_t>(tmp);
    return offset;
  }
  else if constexpr (std::is_same<T, TripleFloat16>::value)
  {
    std::uint32_t tmp1, tmp2;
    std::size_t offset = deserialize(buffer, tmp1);
    offset += deserialize(std::next(buffer, offset), tmp2);
    value.raw = std::array<std::uint16_t, 6>{};
    std::tie(index, value.raw[1]) = unpack<std::uint32_t, std::uint16_t>(tmp1);
    std::tie(value.raw[3], value.raw[5]) =
      unpack<std::uint32_t, std::uint16_t>(tmp2);
    return offset;
  }
  else if constexpr (std::is_same<T, TripleFloat32>::value)
  {
    std::size_t offset = deserialize(buffer, index);
    offset += deserialize(std::next(buffer, offset), value);
    return offset;
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    std::uint32_t tmp;
    std::size_t offset = deserialize(buffer, tmp);
    std::size_t length;
    std::tie(index, length) = unpack<std::uint32_t, std::uint16_t>(tmp);
    for (auto it = value.begin(); it != std::next(value.begin(), length); ++it)
      offset += deserialize(std::next(buffer, offset), *it);
    return offset;
  }
  else
  {
    std::size_t offset = deserialize(buffer, index);
    offset += deserialize(std::next(buffer, offset), value);
    return offset;
  }
}

//! Template specialization for std::array<T, N>
template<class InputIterator, typename T, std::size_t N>
std::size_t
deserialize(const InputIterator& buffer,
            VarIndex_t& index,
            std::array<T, N>& value)
{
  std::size_t offset = deserialize(buffer, index);
  offset += deserialize(std::next(buffer, offset), value);
  return offset;
}

//! Runtime deserialization switch based on TypesMap
template<class InputIterator>
auto
deserializerFactory(std::uint8_t typeCode)
{
  std::function<std::pair<std::size_t, DataTypes>(const InputIterator)>
    deserializer;
  std::size_t alignedTypeSize;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // add one for the type code
                     alignedTypeSize = alignedsize<T>() + 1;
                     deserializer = [=](const InputIterator buffer) {
                       T var;
                       auto offset = deserialize(buffer, var);
                       return std::make_pair(offset, var);
                     };
                   }
                 }));
  return std::make_pair(deserializer, alignedTypeSize);
}

//! Runtime deserialization switch based on TypesMap
template<class InputIterator>
auto
deserializerFactory(VarIndex_t& index, std::uint8_t typeCode)
{
  std::function<std::pair<std::size_t, DataTypes>(const InputIterator)>
    deserializer;
  std::size_t alignedTypeSize;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // add one for the type code
                     alignedTypeSize = alignedsize<T>() + 1;
                     deserializer = [=, &index](const InputIterator buffer) {
                       T var;
                       auto offset = deserialize(buffer, index, var);
                       return std::make_pair(offset, var);
                     };
                   }
                 }));
  return std::make_pair(deserializer, alignedTypeSize);
}

} // namespace common
} // namespace mecos
