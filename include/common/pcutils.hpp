#pragma once
#include "common/serialization.hpp"
#include "common/types.hpp"
#include "common/variable.hpp"
#include <algorithm>
#include <boost/hana.hpp>
#include <iterator>
#include <sstream>
#include <string_view>
#include <type_traits>
#include <vector>

namespace hana = boost::hana;

namespace mecos
{
namespace common
{

using ContainerSize = std::uint32_t;

template<typename T, std::enable_if_t<std::is_arithmetic<T>::value, int> = 0>
T
cast(float level)
{
  return static_cast<T>(level);
}

template<typename T, std::enable_if_t<!std::is_arithmetic<T>::value, int> = 0>
T
cast(float)
{
  return T{};
}

std::vector<std::size_t>
unwrapTime(std::vector<incident::CycleTime> time)
{
  std::vector<std::size_t> unwrappedTime(time.begin(), time.end());
  if (time.size() > 1)
  {
    auto it =
      std::adjacent_find(time.begin(),
                         time.end(),
                         [&](const uint32_t& lhs, const uint32_t& rhs) -> bool {
                           return rhs < lhs;
                         });
    if (it != time.end())
    {
      auto index = std::distance(time.begin(), it);
      for (auto iterator = std::next(unwrappedTime.begin(), index + 1);
           iterator != unwrappedTime.end();
           iterator++)
      {
        (*iterator) += std::numeric_limits<std::uint32_t>::max();
        (*iterator)++;
      }
    }
  }
  return unwrappedTime;
}
std::vector<int32_t>
findSubSamplingIndices(const std::vector<size_t>& time,
                       const uint32_t& samplingTime)
{
  std::vector<int32_t> reduceIndices = { 0 };
  std::size_t offset = 0;
  while (true)
  {
    auto startIterator = std::next(time.begin(), offset);
    auto searchIterator =
      std::find_if(startIterator, time.end(), [&](auto elem) {
        return (elem - *startIterator) == samplingTime;
      });
    if (searchIterator != time.end())
    {
      offset = std::distance(time.begin(), searchIterator);
      reduceIndices.push_back(static_cast<std::int32_t>(offset));
    }
    else
    {
      break;
    }
  }
  return reduceIndices;
}

std::vector<std::string>
split(const std::string& s, char delim)
{
  std::vector<std::string> result;
  std::stringstream ss(s);
  std::string item;

  while (std::getline(ss, item, delim))
  {
    result.push_back(item);
  }

  return result;
}

template<typename T>
void
concatenate(std::vector<T>& a, const std::vector<T>& b)
{
  a.reserve(a.size() + b.size());
  a.insert(a.end(), b.begin(), b.end());
}

template<typename InputIterator, typename Functor>
int
indexIf(InputIterator begin, InputIterator end, Functor lambda)
{
  auto it = std::find_if(begin, end, lambda);
  if (it != end)
  {
    const auto index = std::distance(begin, it);
    return static_cast<int>(index);
  }
  else
  {
    return -1;
  }
}

template<typename InputIterator, typename Functor>
std::vector<int32_t>
indicesIf(InputIterator begin, InputIterator end, Functor lambda)
{
  std::vector<int32_t> matches;
  auto it = begin;
  while (true)
  {
    it = std::find_if(it, end, lambda);
    if (it != end)
    {
      matches.push_back(static_cast<std::int32_t>(std::distance(begin, it)));
      std::advance(it, 1);
    }
    else
    {
      break;
    }
  }
  return matches;
}

size_t
typeCodeToVarSize(const uint8_t typeCode)
{
  size_t varSize = 0;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     varSize = alignedsize<T>();
                   }
                 }));
  return varSize;
}

template<typename Container>
std::uint8_t
varIndexToTypeCode(const VarIndex_t& varIndex, Container& xrt)
{
  if (varIndex == xrt[varIndex].index)
    return xrt[varIndex].code;
  else
    return std::numeric_limits<std::uint8_t>::infinity();
}

template<typename Container>
std::size_t
varIndexToAlignedSize(const VarIndex_t& index, Container& xrt)
{
  std::size_t alignedTypeSize;
  auto typeCode = xrt[index].code;
  hana::for_each(TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     alignedTypeSize = alignedsize<T>();
                   };
                 }));
  return alignedTypeSize;
}

template<typename Container>
std::uint8_t
varNameToTypeCode(const std::string& varName, Container& xrt)
{
  auto index =
    indexIf(xrt.begin(), xrt.end(), [&varName](Variable<std::string> var) {
      return var.name == varName;
    });
  if (index >= 0)
    return xrt[index].code;
  else
    return std::numeric_limits<std::uint8_t>::infinity();
}

template<typename Container>
std::string
varIndexToVarName(const VarIndex_t& varIndex, Container& xrt)
{
  if (varIndex == xrt[varIndex].index)
    return xrt[varIndex].name;
  else
    return std::string{};
}

template<typename Container>
VarIndex_t
varNameToVarIndex(const std::string& varName, Container& xrt)
{
  auto index =
    indexIf(xrt.begin(), xrt.end(), [&varName](Variable<std::string> var) {
      return var.name == varName;
    });
  if (index >= 0)
    return xrt[index].index;
  else
    return std::numeric_limits<VarIndex_t>::max();
}

template<typename T>
using ValueType = typename std::iterator_traits<T>::value_type;

template<typename InputIterator, typename Functor>
auto
filter(InputIterator begin, InputIterator end, Functor lambda)
  -> std::vector<ValueType<InputIterator>>
{
  std::vector<ValueType<InputIterator>> result;
  result.reserve(std::distance(begin, end));
  std::copy_if(begin, end, std::back_inserter(result), lambda);
  return result;
}

template<typename InputIterator, typename Functor>
auto
map(InputIterator begin, InputIterator end, Functor lambda)
{
  using FunctorValueType =
    decltype(std::declval<Functor>()(std::declval<ValueType<InputIterator>>()));

  std::vector<FunctorValueType> result;
  result.reserve(unsigned(std::distance(begin, end)));
  std::transform(begin, end, std::back_inserter(result), lambda);
  return result;
}

const page::DataBase_t*
payloadToBuffer(const std::string_view& contents)
{
  // TODO umb: ACHTUNG hier: Die Länge wird garnicht übermittelt. Führt dies
  // nicht zu Problemen?
  const page::DataBase_t* bufferIt =
    reinterpret_cast<const page::DataBase_t*>(contents.data());
  return bufferIt;
}

template<size_t N>
std::string
bufferToPayload(const std::array<page::DataBase_t, N>& buffer)
{
  const char* charBytes = reinterpret_cast<const char*>(buffer.data());
  std::string payload(charBytes, buffer.size() * sizeof(page::DataBase_t));
  return payload;
}
std::string
bufferToPayload(const std::vector<page::DataBase_t>& buffer)
{
  const char* charBytes = reinterpret_cast<const char*>(buffer.data());
  std::string payload(charBytes, buffer.size() * sizeof(page::DataBase_t));
  return payload;
}
std::string
bufferToPayload(const std::vector<std::byte>& buffer)
{
  const char* charBytes = reinterpret_cast<const char*>(buffer.data());
  std::string payload(charBytes, buffer.size());
  return payload;
}

std::vector<Variable<std::string>>
bufferToXrt(const page::DataBase_t* buffer)
{
  std::vector<Variable<std::string>> xrt;
  std::uint32_t xrtsize;
  deserialize(buffer, xrtsize);
  xrt.resize(xrtsize);
  deserialize(buffer, xrt);
  return xrt;
}

std::vector<incident::Definition<std::string>>
bufferToIncidentTable(const page::DataBase_t* buffer)
{
  std::vector<incident::Definition<std::string>> incidentTable;
  std::uint32_t incidentTableSize;
  deserialize(buffer, incidentTableSize);
  incidentTable.resize(incidentTableSize);
  deserialize(buffer, incidentTable);
  return incidentTable;
}
} // namespace common
} // namespace mecos
