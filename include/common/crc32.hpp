#pragma once
#include <cstdint>
#include <type_traits>

namespace mecos
{
namespace common
{

enum class crc32 : std::uint32_t
{
};

template<class IntegerType>
constexpr IntegerType
to_integer(crc32 crc) noexcept
{
  static_assert(std::is_integral<IntegerType>::value, "Integral required.");
  return static_cast<IntegerType>(crc);
}

//! Standard ethernet CRC 32 calculation
/*! bit_count: 32
 *  truncated_polynomial: 0x04C11DB7
 *  initial_remainder: 0xFFFFFFFF
 *  final_xor_value: 0xFFFFFFFF
 *  reflect_input: true
 *  reflect_output: true
 *  see: https://www.boost.org/doc/libs/1_74_0/doc/html/crc/introduction.html
 */
crc32
crc32_calc(const void* data, std::size_t size);

// buffer is pointer to data to generate syndrome for
//  length is the number of words (2 bytes) in the buffer.
//
// This routine generates a 32 bit syndrome for the passed data
//
// Return Value:
// Generated 32 bit forward syndrome.
crc32
ecc_gen_fwd_syn32(const void* data, std::size_t size);

} // common
} // mecos
