#pragma once
#include "common/crc32.hpp"
#include "common/incident.hpp"
#include "common/utility.hpp"
#include "common/variable.hpp"
#include "common/version.hpp"
#include <array>
#include <boost/hana.hpp>
#include <cmath>
#include <functional>
#include <numeric>
#include <utility>
#include <variant>

namespace mecos
{
namespace common
{

namespace hana = boost::hana;

enum class ErrorCode : std::uint16_t
{
  ok,
  listIsFull,
  invalidCopyMethod,
  invalidType,
  invalidVarIndex,
  invalidPageHeader,
  invalidXrt,
  SelfcheckDisplay,
  AccessViolation,
  PageError,
  OutOfRange
};

namespace page
{
typedef uint32_t DataBase_t;
} // type of page buffer array

struct GeneratorParameters
{
  BOOST_HANA_DEFINE_STRUCT(GeneratorParameters,
                           (float, Frequency),
                           (float, Slope));
};

struct SinGenParameters
{
  BOOST_HANA_DEFINE_STRUCT(SinGenParameters,
                           (std::uint32_t, Index),
                           (std::uint32_t, LeaseTime),
                           (float, AmplitudeCos),
                           (float, AmplitudeSin));
};

struct FindLogdata // ToDo: Bue This should be replaced by something similar
                   // like FindIncidentData and ReadIncidentData, TBD
{
  BOOST_HANA_DEFINE_STRUCT(FindLogdata,
                           (incident::Timestamp, TimeOfData),
                           (std::uint16_t, NumberOfData));
};

struct ReadvarData
{
  BOOST_HANA_DEFINE_STRUCT(ReadvarData,
                           (VarIndex_t, VariableIndex),
                           (std::uint32_t, LeaseTime),
                           (std::uint32_t, SamplingTime));
};

struct LogdataSpecification
{
  BOOST_HANA_DEFINE_STRUCT(LogdataSpecification,
                           (VarIndex_t, VariableIndex),
                           (std::uint32_t, SamplingTime));
};

using FindIncidentData = incident::FindSpecification;
using ReadIncidentData = incident::ReadSpecification; // used by DisplayBroker
using IncidentLogData = incident::LogDefinition;
using IncidentLevels = incident::Levels;
using DisplayVersion = std::array<char, 20>; // ToDo: Obsolete
using SerialNumber = std::array<char, 6>;
using Text = std::array<char, 37>;

using TripleFloat32 = std::array<float, 3>;
union TripleFloat16
{
  TripleFloat32 float32;
  // serialize only 16bit of float (7-bit resolution)  ToDo
  std::array<std::uint16_t, 6> raw;
};

struct SetCatchRPC
{ // used for Display only. Divert error messages
  BOOST_HANA_DEFINE_STRUCT(SetCatchRPC, (std::array<char, 30>, catchString));
};

//! Mapping between type and type code as a single source of truth
// clang-format off
constexpr auto TypesMap = hana::make_tuple(
  hana::make_pair(hana::type_c<std::uint32_t>,        uint8_t_c<0xce>),
  hana::make_pair(hana::type_c<std::int32_t>,         uint8_t_c<0x2d>),
  hana::make_pair(hana::type_c<std::uint16_t>,        uint8_t_c<0xcd>),
  hana::make_pair(hana::type_c<std::int16_t>,         uint8_t_c<0xd1>),
  hana::make_pair(hana::type_c<std::uint8_t>,         uint8_t_c<0xcc>),
  hana::make_pair(hana::type_c<std::int8_t>,          uint8_t_c<0xd0>),
  hana::make_pair(hana::type_c<float>,                uint8_t_c<0xca>),
  hana::make_pair(hana::type_c<bool>,                 uint8_t_c<0xc0>),
  hana::make_pair(hana::type_c<FindIncidentData>,     uint8_t_c<0x01>),
  hana::make_pair(hana::type_c<SinGenParameters>,     uint8_t_c<0x02>),
  hana::make_pair(hana::type_c<FindLogdata>,          uint8_t_c<0x03>),
  hana::make_pair(hana::type_c<ReadvarData>,          uint8_t_c<0x04>),
  hana::make_pair(hana::type_c<DisplayVersion>,       uint8_t_c<0x05>),
  hana::make_pair(hana::type_c<ErrorCode>,            uint8_t_c<0x06>),
  hana::make_pair(hana::type_c<TripleFloat32>,        uint8_t_c<0x07>),
  hana::make_pair(hana::type_c<TripleFloat16>,        uint8_t_c<0x08>),
  hana::make_pair(hana::type_c<DateTime>,             uint8_t_c<0x09>),
  hana::make_pair(hana::type_c<SetCatchRPC>,          uint8_t_c<0x0a>),
  hana::make_pair(hana::type_c<GeneratorParameters>,  uint8_t_c<0x0b>),
  hana::make_pair(hana::type_c<IncidentLogData>,      uint8_t_c<0x0c>),
  hana::make_pair(hana::type_c<LogdataSpecification>, uint8_t_c<0x0d>),
  hana::make_pair(hana::type_c<SerialNumber>,         uint8_t_c<0x0e>),
  hana::make_pair(hana::type_c<Text>,                 uint8_t_c<0x0f>),
  hana::make_pair(hana::type_c<Version>,              uint8_t_c<0x10>),
  hana::make_pair(hana::type_c<ReadIncidentData>,     uint8_t_c<0x11>)
  /* hana::make_pair(hana::type_c<IncidentLevels>,       uint8_t_c<0x12>) */
  /* hana::make_pair(hana::type_c<VarIndex_t>,           uint8_t_c<0x13>) */
);
// clang-format on

//! Variant with types from TypesMap
using DataTypes = typename toVariant<decltype(removeTypeCode(TypesMap))>::type;

//! Variant with pointer types from TypesMap
using DataTypesPointer =
  typename toVariant<decltype(addPointer(removeTypeCode(TypesMap)))>::type;

} // namespace common
} // namespace mecos
