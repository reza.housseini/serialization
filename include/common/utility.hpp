#pragma once
#include <boost/hana.hpp>
#include <type_traits>
#include <variant>

namespace mecos
{
namespace common
{

namespace hana = boost::hana;

template<typename T>
struct is_stl_container_like
{
  typedef typename std::remove_const<T>::type test_type;

  template<typename A>
  static constexpr bool test(A* pt,
                             A const* cpt = nullptr,
                             decltype(pt->begin())* = nullptr,
                             decltype(pt->end())* = nullptr,
                             decltype(cpt->begin())* = nullptr,
                             decltype(cpt->end())* = nullptr)
  {
    typedef typename A::iterator iterator;
    typedef typename A::const_iterator const_iterator;
    return std::is_same<decltype(pt->begin()), iterator>::value &&
           std::is_same<decltype(pt->end()), iterator>::value &&
           std::is_same<decltype(cpt->begin()), const_iterator>::value &&
           std::is_same<decltype(cpt->end()), const_iterator>::value;
  }

  template<typename A>
  static constexpr bool test(...)
  {
    return false;
  }

  static const bool value = test<test_type>(nullptr);
};

auto removeTypeCode = [](auto map) {
  return hana::transform(hana::to_tuple(map),
                         [](auto pair) { return hana::first(pair); });
};

auto addPointer = [](auto tuple) {
  return hana::transform(
    tuple, [](auto item) { return hana::traits::add_pointer(item); });
};

template<typename Tuple>
struct toVariant;

template<typename... Ts>
struct toVariant<hana::tuple<Ts...>>
{
  using type = std::variant<typename Ts::type...>;
};

template<typename Type, typename T>
constexpr decltype(auto)
getTypeCode(T map)
{
  auto found = hana::index_if(map, [](auto const& pair) {
    return hana::first(pair) == hana::type_c<Type>;
  });
  return hana::second(hana::at(map, found.value()));
}

//! @relates hana::integral_constant
template<std::uint8_t c>
using uint8_t_ = hana::integral_constant<std::uint8_t, c>;

//! @relates hana::integral_constant
template<std::uint8_t c>
constexpr uint8_t_<c> uint8_t_c{};

} // namespace common
} // namespace mecos
