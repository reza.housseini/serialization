#pragma once
#include "common/incident.hpp"
#include "common/utility.hpp"

namespace mecos
{
namespace common
{

template<typename U>
void
fill(incident::Timestamp& t, U value)
{
  static_assert(std::is_arithmetic<U>::value, "Arithmetic required.");
  t.DateAndTime.time.Sec = value;
  t.DateAndTime.time.Min = value;
  t.DateAndTime.time.Hour = value;
  t.DateAndTime.time.Day = value;
  t.DateAndTime.time.Month = value;
  t.DateAndTime.time.Year = value;
  t.CycleTime = value;
};

template<typename T, typename U>
void
fill(T& t, U value)
{
  static_assert(std::is_arithmetic<U>::value, "Arithmetic required.");
  if constexpr (std::is_arithmetic<T>::value or std::is_enum<T>::value)
    t = static_cast<T>(value);
  else if constexpr (is_stl_container_like<T>::value)
  {
    for (auto& item : t)
      fill(item, value);
  }
  else if constexpr (std::is_class<T>::value)
  {
    hana::for_each(hana::accessors<T>(), [&](auto&& accessor) {
      auto get_member = hana::second(accessor);
      fill(get_member(t), value);
    });
  }
};

template<typename T>
bool
compare(T& t, T& u)
{
  if constexpr (std::is_arithmetic<T>::value or std::is_enum<T>::value)
  {
    return t == u;
  }
  else if constexpr (is_stl_container_like<T>::value)
  {
    for (auto it = t.begin(); it != t.end(); ++it)
    {
      auto d = std::distance(t.begin(), it);
      bool tmp = compare(*it, *std::next(u.begin(), d));
      if (!tmp)
        return tmp;
    }
    return true;
  }
  else if constexpr (std::is_class<T>::value)
  {
    return hana::fold_left(t, true, [&](auto total, auto member) {
      auto tmp = hana::find(u, hana::first(member)).value();
      return total and compare(hana::second(member), tmp);
    });
  }
};

template<>
bool
compare(incident::Timestamp& t, incident::Timestamp& u)
{
  return t.DateAndTime.time.Sec == u.DateAndTime.time.Sec and
         t.DateAndTime.time.Min == u.DateAndTime.time.Min and
         t.DateAndTime.time.Hour == u.DateAndTime.time.Hour and
         t.DateAndTime.time.Day == u.DateAndTime.time.Day and
         t.DateAndTime.time.Month == u.DateAndTime.time.Month and
         t.DateAndTime.time.Year == u.DateAndTime.time.Year and
         t.CycleTime == u.CycleTime;
};

} // common
} // mecos
