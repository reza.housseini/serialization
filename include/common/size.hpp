#pragma once
#include "common/types.hpp"
#include <boost/hana.hpp>

namespace mecos
{
namespace common
{

namespace hana = boost::hana;

// Get number of elements of an array
// Returns 1 if it is not an array
inline int32_t
getNumElements(uint8_t typeCode)
{
  int32_t NumElements;
  hana::for_each(common::TypesMap, hana::fuse([&](auto type, auto code) {
                   if (typeCode == code)
                   {
                     using T = typename decltype(type)::type;
                     // StartIndex: Find entries >=StartIndex only
                     if constexpr (common::is_stl_container_like<T>::value)
                     {
                       NumElements = std::tuple_size<T>::value;
                     }
                     else
                     {
                       NumElements = 1;
                     }
                   }
                 }));
  return NumElements;
}


template<typename Dividend, typename Divisor>
constexpr auto
div_ceil(Dividend x, Divisor y)
{
  if constexpr (std::is_unsigned_v<Dividend> && std::is_unsigned_v<Divisor>)
  {
    // quotient is always positive
    return x / y + (x % y != 0); // uint / uint
  }
  else if constexpr (std::is_signed_v<Dividend> && std::is_unsigned_v<Divisor>)
  {
    auto sy = static_cast<std::make_signed_t<Divisor>>(y);
    bool quotientPositive = x >= 0;
    return x / sy + (x % sy != 0 && quotientPositive); // int / uint
  }
  else if constexpr (std::is_unsigned_v<Dividend> && std::is_signed_v<Divisor>)
  {
    auto sx = static_cast<std::make_signed_t<Dividend>>(x);
    bool quotientPositive = y >= 0;
    return sx / y + (sx % y != 0 && quotientPositive); // uint / int
  }
  else
  {
    bool quotientPositive = (y >= 0) == (x >= 0);
    return x / y + (x % y != 0 && quotientPositive); // int / int
  }
}

template<typename T>
constexpr std::size_t
bytesize()
{
  if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
    return sizeof(T);
  else if constexpr (is_stl_container_like<T>::value)
  {
    using element_type =
      std::remove_reference_t<decltype(*std::begin(std::declval<T&>()))>;
    return std::tuple_size<T>::value * bytesize<element_type>();
  }
  else if constexpr (std::is_class<T>::value)
  {
    return hana::fold_left(
      hana::accessors<T>(), 0, [](auto total, auto member) {
        using member_type =
          std::decay_t<decltype(hana::second(member)(std::declval<T>()))>;
        constexpr std::size_t member_size = bytesize<member_type>();
        return member_size + total;
      });
  }
}

// Template specialization for DateTime
template<>
constexpr std::size_t
bytesize<DateTime>()
{
  return sizeof(DateTime);
}

// Template specialization for TripleFloat16
template<>
constexpr std::size_t
bytesize<TripleFloat16>()
{
  return sizeof(uint16_t)*3;
}

template<typename T>
constexpr std::size_t
bytesize(const T& t)
{
  if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
    return sizeof(T);
  else if constexpr (is_stl_container_like<T>::value)
  {
    return t.size() * bytesize(*t.cbegin());
  }
  else if constexpr (std::is_class<T>::value)
  {
    return hana::fold_left(
      hana::accessors<T>(), 0, [&t](auto total, auto member) {
        std::size_t member_size = bytesize(hana::second(member)(t));
        return member_size + total;
      });
  }
}

// Template specialization for DateTime
template<>
constexpr std::size_t
bytesize<DateTime>(const DateTime&)
{
  return sizeof(DateTime);
}

// Template specialization for TripleFloat16
template<>
constexpr std::size_t
bytesize<TripleFloat16>(const TripleFloat16&)
{
  return sizeof(uint16_t)*3;
}

template<typename T>
constexpr std::size_t
alignedsize()
{
  if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
    return div_ceil(bytesize<T>(), sizeof(page::DataBase_t));
  else if constexpr (is_stl_container_like<T>::value)
  {
    using element_type =
      std::remove_reference_t<decltype(*std::begin(std::declval<T&>()))>;
    return std::tuple_size<T>::value * alignedsize<element_type>();
  }
  else if constexpr (std::is_class<T>::value)
  {
    return hana::fold_left(
      hana::accessors<T>(), 0, [](auto total, auto member) {
        using member_type =
          std::decay_t<decltype(hana::second(member)(std::declval<T>()))>;
        constexpr std::size_t member_size = alignedsize<member_type>();
        return member_size + total;
      });
  }
}

// Template specialization for DateTime
template<>
constexpr std::size_t
alignedsize<DateTime>()
{
  return div_ceil(bytesize<DateTime>(), sizeof(page::DataBase_t));
}

// Template specialization for TripleFloat16
template<>
constexpr std::size_t
alignedsize<TripleFloat16>()
{
  return div_ceil(bytesize<TripleFloat16>(), sizeof(page::DataBase_t));
}

template<typename T>
constexpr std::size_t
alignedsize(const T& t)
{
  if constexpr (std::is_arithmetic<T>::value || std::is_enum<T>::value)
    return div_ceil(bytesize(t), sizeof(page::DataBase_t));
  else if constexpr (is_stl_container_like<T>::value)
  {
    size_t size = 0;
    for (auto& item : t)
      size += alignedsize(item);
    return size;
  }
  else if constexpr (std::is_class<T>::value)
  {
    return hana::fold_left(
      hana::accessors<T>(), 0, [&t](auto total, auto member) {
        std::size_t member_size = alignedsize(hana::second(member)(t));
        return member_size + total;
      });
  }
}

// Template specialization for DateTime
template<>
constexpr std::size_t
alignedsize<DateTime>(const DateTime& t)
{
  return div_ceil(bytesize(t), sizeof(page::DataBase_t));
}

// Template specialization for TripleFloat16
template<>
constexpr std::size_t
alignedsize<TripleFloat16>(const TripleFloat16& t)
{
  return div_ceil(bytesize(t), sizeof(page::DataBase_t));
}

} // namespace common
} // namespace mecos
