#pragma once
#include <boost/hana.hpp>
#include <cstdint>
#include <string_view>

namespace mecos
{
namespace common
{

namespace hana = boost::hana;

struct Range
{
  BOOST_HANA_DEFINE_STRUCT(Range, (float, lower), (float, upper));
};

enum class Rate : std::uint32_t
{
  fast = 40000,
  slow = 1000,
  zero = 0
};

enum class Unit : std::uint32_t
{
  ampere,
  volt,
  meter,
  kilo,
  hertz,
  celcius,
  percent,
  ticks,
  none
};

typedef uint16_t VarIndex_t; // Das muss uint16_t sein
static const VarIndex_t InvalidVarIndex = 0xFFFF;

//! Platform independent Variable definition
template<typename StringType = std::string_view>
struct Variable
{
  BOOST_HANA_DEFINE_STRUCT(
    Variable,
    (VarIndex_t, index),
    //! Name of the variable.
    /*! Needs to be unique and follow the naming convention
     *  specified in the <a href="https://gitlab-zur.md-man.biz/mecos/alosa1/-/\
     *  jobs/artifacts/develop/browse/doc?job=build-doc">
     *  software design description</a> document
     */
    (StringType, name),
    //! Valid range of the variable
    (Range, range),
    //! Type code of the variable
    (std::uint8_t, code),
    //! Update rate of the variable
    (Rate, rate),
    //! SI unit of the variable
    (Unit, unit),
    //! Info text of the variable
    (StringType, info));
};

} // namespace common
} // namespace mecos
