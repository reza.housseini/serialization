# Building

The code is buildable for the MinGW and the ARM target. Therefore two cross
files are provided `arm-none-eabi.config` for the ARM build and 
`windows-mingw-static.config` for the MinGW build.
To disallow the heap usage, the compiler generates an error message when 
`malloc` is used (either from a library or standalone).

## Building for Windows

    docker run --rm -v $(pwd -P):/usr/project rezahousseini/windows-mingw-toolchain:latest meson build --cross-file windows-mingw-static.config --buildtype=debug --default-library=both

## Building for ARM

    docker run --rm -v $(pwd -P):/usr/project rezahousseini/arm-none-eabi-toolchain:latest meson build --cross-file arm-none-eabi.config --buildtype=debug --default-library=both

# Testing

## Coverage report

A test coverage report can be found [here](http://mecos.gitlabpages-zur.md-man.biz:8090/alosa/common/coveragereport/).

# Documentation

## Doxygen

A doxygen generated documenation can be found [here](http://mecos.gitlabpages-zur.md-man.biz:8090/alosa/common/doxygen/).
